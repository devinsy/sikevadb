# Welcome

SikevaDB is a Simple KEy VAlue DataBase.

Features:
* double key value
* single session only
* connector to file persistence
* connector to SQL persistence

Why build a single session database? Because, in so many cases, I need a single session only in my application.

Actually, the following backends are supported:
* file system
* MySQL Database
* HSQL Database

# LICENSE

SikevaDB is released under the GNU AGPL license. Enjoy!

# AUTHOR

Christian Pierre MOMON <christian.momon@devinsy.fr>

# Developer install

TODO

## Unit test environment
For unit tests, install the TestNG: 
* https://marketplace.eclipse.org/content/testng-eclipse
* Eclipse menu > Help > Eclipse Marketplace > Find "TestNG" > TestNG for Eclipse: Install button

# LOGO
Author: Christian Pierre MOMON <christian.momon@devinsy.fr>
License: Creative Commons BY-ND-SA
Made with: Image from LibreOffice