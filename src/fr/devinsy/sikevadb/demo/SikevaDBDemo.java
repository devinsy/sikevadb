/*
 * Copyright (C) 2013-2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.devinsy.sikevadb.demo;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.BuildInformation;
import fr.devinsy.sikevadb.core.SikevaDB;
import fr.devinsy.sikevadb.core.SikevaDBException;
import fr.devinsy.sikevadb.core.SikevaDBFactory;
import fr.devinsy.util.strings.StringList;

/**
 * The Class SikevaDBDemo is a demo for SikevaDB.
 */
public final class SikevaDBDemo
{
	private static Logger logger = LoggerFactory.getLogger(SikevaDBDemo.class);

	/**
	 * SikevaDBDemo class has to be not instanciated.
	 */
	private SikevaDBDemo()
	{
	}

	/**
	 * Demo.
	 * 
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public static void demo() throws SikevaDBException
	{
		SikevaDB database = null;
		try
		{
			//
			File homeDirectory = new File("/tmp/footest");
			database = SikevaDBFactory.get(homeDirectory);

			if (database.exists())
			{
				database.destroy();
			}

			database.create();
			database.open();

			// Single key value.
			{
				database.put("alpha", "alpha value");
				database.put("bravo", "bravo value");
				database.put("charlie", "charlie value");

				String value = database.getValue("alpha");
				if (StringUtils.equals(value, "alpha value"))
				{
					System.out.println("alpha is correctly stored.");
				}
				else
				{
					System.out.println("alpha is NOT correctly stored.");
				}

				//
				database.delete("alpha");
				if (database.getValue("alpha") == null)
				{
					System.out.println("alpha is correctly deleted.");
				}
				else
				{
					System.out.println("alpha is NOT correctly deleted.");
				}
			}

			// Double key values.
			{
				for (int index = 0; index < 10; index++)
				{
					database.put("victor", index, "victor" + index);
				}

				String value = database.getValue("victor", 7);
				if (StringUtils.equals(value, "victor7"))
				{
					System.out.println("victor is correctly stored.");
				}
				else
				{
					System.out.println("victor is NOT correctly stored.");
				}

				//
				database.delete("victor", 5);
				if (database.getValue("victor", 5) == null)
				{
					System.out.println("(victor, 5) is correctly deleted.");
				}
				else
				{
					System.out.println("(victor, 5) is NOT correctly deleted.");
				}
			}

			//
			System.out.println(String.format("Database values:\t %d elements", database.countOfElements()));
			System.out.println(String.format("Database memory:\t %d bytes", database.memorySize()));

			//
			database.close();
		}
		finally
		{
			if ((database != null) && (database.exists()))
			{
				database.destroy();
			}
		}
	}

	/**
	 * This method displays the CLI help.
	 * 
	 */
	public static void help()
	{
		StringList message = new StringList();

		message.append("SikevaDB Demo version ").appendln(BuildInformation.instance().version());
		message.appendln("Usage:");
		message.appendln("    sikevadbdemo [ -h | -help | --help ]");
		message.appendln("    sikevadb -demo [ -h | -help | --help ]");

		System.out.println(message.toString());
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		// Configure log.
		File loggerConfig = new File("log4j.properties");
		if (loggerConfig.exists())
		{
			PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
			logger.info("Dedicated log configuration done.");
			logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
		}
		else
		{
			BasicConfigurator.configure();
			logger.info("Basic log configuration done.");
			logger.info("Configuration file was not found in [{}].", loggerConfig.getAbsoluteFile());
		}

		// Run.
		SikevaDBDemo.run(args);
	}

	/**
	 * 
	 * This method launch CLI.
	 * 
	 * @param args
	 *            necessary arguments
	 */
	public static void run(final String[] args)
	{
		try
		{
			// Set default catch.
			Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
			{
				@Override
				public void uncaughtException(final Thread thread, final Throwable exception)
				{
					String message;
					if (exception instanceof OutOfMemoryError)
					{
						message = "Java ran out of memory!\n\n";
					}
					else
					{
						message = String.format("An error occured: %1s(%2s)", exception.getClass(), exception.getMessage());
					}

					logger.error("uncaughtException ", exception);
					logger.error(message);
					logger.info("Oups, an unexpected error occured. Please try again.");
				}
			});

			// This part implements an automate.
			int parameterCount = args.length;
			if (parameterCount == 0)
			{
				demo();
			}
			else if (StringUtils.equals(args[0], "-h") || StringUtils.equals(args[0], "-help") || StringUtils.equals(args[0], "--help"))
			{
				help();
			}
			else if (StringUtils.equals(args[0], "get"))
			{
				switch (parameterCount)
				{
					case 2:
					{
					}
					break;

					case 3:
					{
					}
					break;

					default:
						throw new SikevaDBException("Bad parameter count.");
				}
			}
			else
			{
				System.out.println("Bad usage detected.");
				help();
			}
		}
		catch (SikevaDBException exception)
		{
			System.err.println("SibaException = " + exception.getMessage());
			logger.error(exception.getMessage(), exception);
			help();
		}
	}
}
