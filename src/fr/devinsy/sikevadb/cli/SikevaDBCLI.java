/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.devinsy.sikevadb.cli;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.BuildInformation;
import fr.devinsy.sikevadb.core.SikevaDBException;
import fr.devinsy.util.strings.StringList;

/**
 * The Class <code>SikevaDBCLI</code> manages a Command Line Interface for
 * SikevaDB.
 * 
 */
public final class SikevaDBCLI
{
	private static Logger logger = LoggerFactory.getLogger(SikevaDBCLI.class);

	/**
	 * Instantiates a new SikevaDBCLI.
	 */
	private SikevaDBCLI()
	{
	}

	/**
	 * This method displays the CLI help.
	 * 
	 */
	public static void help()
	{
		StringList message = new StringList();

		message.append("SikevaDB CLI version ").appendln(BuildInformation.instance().version());
		message.appendln("Usage:");
		message.appendln("    sikevadbcli [ -h | -help | --help ]");
		message.appendln("    sikevadbcli get key");
		message.appendln("    sikevadbcli get key subkey");

		System.out.println(message.toString());
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		// Configure log.
		File loggerConfig = new File("log4j.properties");
		if (loggerConfig.exists())
		{
			PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
			logger.info("Dedicated log configuration done.");
			logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
		}
		else
		{
			BasicConfigurator.configure();
			logger.info("Basic log configuration done.");
			logger.info("Configuration file was not found in [{}].", loggerConfig.getAbsoluteFile());
		}

		// Run.
		SikevaDBCLI.run(args);
	}

	/**
	 * 
	 * This method launch CLI.
	 * 
	 * @param args
	 *            necessary arguments
	 */
	public static void run(final String[] args)
	{
		try
		{
			// Set default catch.
			Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
			{
				@Override
				public void uncaughtException(final Thread thread, final Throwable exception)
				{
					String message;
					if (exception instanceof OutOfMemoryError)
					{
						message = "Java ran out of memory!\n\n";
					}
					else
					{
						message = String.format("An error occured: %1s(%2s)", exception.getClass(), exception.getMessage());
					}

					logger.error("uncaughtException ", exception);
					logger.error(message);
					logger.info("Oups, an unexpected error occured. Please try again.");
				}
			});

			// This part implements an automate.
			int parameterCount = args.length;
			if (parameterCount == 0)
			{
				help();
			}
			else if (StringUtils.equals(args[0], "-h") || StringUtils.equals(args[0], "-help") || StringUtils.equals(args[0], "--help"))
			{
				help();
			}
			else if (StringUtils.equals(args[0], "get"))
			{
				switch (parameterCount)
				{
					case 2:
					{
					}
					break;

					case 3:
					{
					}
					break;

					default:
						throw new SikevaDBException("Bad parameter count.");
				}
			}
			else
			{
				System.out.println("Bad usage detected.");
				help();
			}
		}
		catch (SikevaDBException exception)
		{
			System.err.println("SikevaDBException = " + exception.getMessage());
			logger.error(exception.getMessage(), exception);
			help();
		}
	}
}
