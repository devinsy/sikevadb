/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.cli;

import fr.devinsy.sikevadb.core.SikevaDBException;

/**
 * The Class SikevaDBCLIException.
 */
public class SikevaDBCLIException extends SikevaDBException
{
	private static final long serialVersionUID = 2986878456227891377L;

	/**
	 * Instantiates a new SikevaDBCLI exception.
	 */
	public SikevaDBCLIException()
	{
		super();
	}

	/**
	 * Instantiates a new SikevaDBCLI exception.
	 * 
	 * @param message
	 *            the message
	 */
	public SikevaDBCLIException(final String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new SikevaDBCLI exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public SikevaDBCLIException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Instantiates a new SikevaDBCLI exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public SikevaDBCLIException(final Throwable cause)
	{
		super(cause);
	}
}