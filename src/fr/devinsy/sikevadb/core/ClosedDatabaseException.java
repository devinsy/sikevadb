/*
 * Copyright (C) 2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.core;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class ClosedDatabaseException extends SikevaDBException
{
	private static final long serialVersionUID = 8364599416669077052L;

	/**
	 * 
	 */
	public ClosedDatabaseException()
	{
		super("Invalid database status for this operation: closed.");
	}

	/**
	 * 
	 * @param message
	 */
	public ClosedDatabaseException(final String message)
	{
		super(message);
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public ClosedDatabaseException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * 
	 * @param cause
	 */
	public ClosedDatabaseException(final Throwable cause)
	{
		super(cause);
	}
}
