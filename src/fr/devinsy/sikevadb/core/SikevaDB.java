/*
 * Copyright (C) 2013-2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.core;

import fr.devinsy.util.strings.StringList;

/**
 * The Interface SikevaDB.
 * 
 * @author Christian Pierre MOMON
 */
public interface SikevaDB
{
	/**
	 * Archiver.
	 *
	 * @return the archiver
	 * @throws SikevaDBException
	 *             the sikeva DB exception
	 */
	public Archiver archiver() throws SikevaDBException;

	/**
	 * Delete all elements of current opened database.
	 *
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public void clear() throws SikevaDBException;

	/**
	 * Close the current database session.
	 *
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public void close() throws SikevaDBException;

	/**
	 * Count of elements in current database.
	 *
	 * @return the long
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public long countOfElements() throws SikevaDBException;

	/**
	 * Count of elements by key.
	 *
	 * @param key
	 *            the key
	 * @return the long
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public long countOfElements(String key) throws SikevaDBException;

	/**
	 * Count of elements by key and sub key.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @return the long
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public long countOfElements(String key, long subkey) throws SikevaDBException;

	/**
	 * Count of elements by key and sub key.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the subkey
	 * @return the long
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public long countOfElements(String key, String subkey) throws SikevaDBException;

	/**
	 * Creates the database.
	 *
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public void create() throws SikevaDBException;

	/**
	 * Delete an database element with its key.
	 *
	 * @param key
	 *            the key
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void delete(final String key) throws SikevaDBException;

	/**
	 * Delete an database element with its key and subkey.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void delete(final String key, final long subkey) throws SikevaDBException;

	/**
	 * Delete an database element with its key and subkey.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void delete(final String key, final String subkey) throws SikevaDBException;

	/**
	 * Delete many database elements from key and sub keys.
	 *
	 * @param key
	 *            the key
	 * @param subkeys
	 *            the subkeys
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void deleteMany(final String key, final String... subkeys) throws SikevaDBException;

	/**
	 * Destroy.
	 *
	 * @throws SikevaDBException
	 *             the sikeva DB exception
	 */
	public void destroy() throws SikevaDBException;

	/**
	 * Gets an element by key.
	 *
	 * @param key
	 *            the key
	 * @return the element
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public Element getElement(String key) throws SikevaDBException;

	/**
	 * Gets an element by key and sub key.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @return the element
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public Element getElement(String key, long subkey) throws SikevaDBException;

	/**
	 * Gets an element by key and sub key.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @return the element
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public Element getElement(String key, String subkey) throws SikevaDBException;

	/**
	 * Gets all elements in current database.
	 *
	 * @return the elements
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public Elements getElements() throws SikevaDBException;

	/**
	 * Gets all elements of a top key.
	 *
	 * @param key
	 *            the key
	 * @return the elements
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public Elements getElements(String key) throws SikevaDBException;

	/**
	 * Gets all the top keys.
	 *
	 * @return the keys
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public StringList getTopKeys() throws SikevaDBException;

	/**
	 * Gets all the sub keys of a key.
	 *
	 * @param key
	 *            the key
	 * @return the subkeys
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public StringList getSubkeys(String key) throws SikevaDBException;

	/**
	 * Gets the value of a key.
	 *
	 * @param key
	 *            the key
	 * @return the value
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public String getValue(String key) throws SikevaDBException;

	/**
	 * Gets the value of a key and a sub key.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @return the value
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public String getValue(String key, long subkey) throws SikevaDBException;

	/**
	 * Gets the value of a key and a sub key.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @return the value
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public String getValue(String key, String subkey) throws SikevaDBException;

	/**
	 * Gets the values of a key.
	 *
	 * @param key
	 *            the key
	 * @return the values
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public StringList getValues(String key) throws SikevaDBException;

	boolean isArchiveActivated();

	boolean isArchiveSuspended();

	/**
	 * Checks if is closed.
	 *
	 * @return true, if is closed
	 */
	public boolean isClosed();

	/**
	 * Checks if is created.
	 *
	 * @return true, if is created
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public boolean exists() throws SikevaDBException;

	/**
	 * Checks if is opened.
	 *
	 * @return true, if is opened
	 */
	public boolean isOpened();

	/**
	 * Returns the memory size of the elements in database.
	 *
	 * @return the long
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public long memorySize() throws SikevaDBException;

	/**
	 * Returns the memory size of elements of a key.
	 *
	 * @param key
	 *            the key
	 * @return the long
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public long memorySize(String key) throws SikevaDBException;

	/**
	 * Returns the memory size of element of a key and a sub key.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @return the long
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public long memorySize(String key, long subkey) throws SikevaDBException;

	/**
	 * Returns the memory size of element of a key and a sub key.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @return the long
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public long memorySize(String key, String subkey) throws SikevaDBException;

	/**
	 * Open a database.
	 *
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void open() throws SikevaDBException;

	/**
	 * Put an element in database.
	 *
	 * @param element
	 *            the element
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void put(Element element) throws SikevaDBException;

	/**
	 * Put a value for a key and a sub key in current opened database.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @param value
	 *            the value
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void put(String key, long subkey, String value) throws SikevaDBException;

	/**
	 * Put a value for a key in current opened database.
	 *
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void put(String key, String value) throws SikevaDBException;

	/**
	 * Put a value for a key and a sub key in current opened database.
	 *
	 * @param key
	 *            the key
	 * @param subkey
	 *            the sub key
	 * @param value
	 *            the value
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void put(String key, String subkey, String value) throws SikevaDBException;

	/**
	 * Rename a key.
	 *
	 * @param oldKey
	 *            the old key
	 * @param newKey
	 *            the new key
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void renameKey(final String oldKey, final String newKey) throws SikevaDBException;

	/**
	 * Rename a sub key.
	 *
	 * @param key
	 *            the key
	 * @param oldSubKey
	 *            the old sub key
	 * @param newSubKey
	 *            the new sub key
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void renameSubkey(final String key, final long oldSubKey, final long newSubKey) throws SikevaDBException;

	/**
	 * Rename a sub key.
	 *
	 * @param key
	 *            the key
	 * @param oldSubKey
	 *            the old sub key
	 * @param newSubKey
	 *            the new sub key
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void renameSubkey(final String key, final String oldSubKey, final String newSubKey) throws SikevaDBException;

	/**
	 * Replace a substring in a value of a key.
	 *
	 * @param key
	 *            the key
	 * @param tokens
	 *            the tokens
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void replaceInValue(final String key, final String... tokens) throws SikevaDBException;

	/**
	 * Replace a substring in values of all sub keys of a key.
	 *
	 * @param key
	 *            the key
	 * @param tokens
	 *            the tokens
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	void replaceInValues(final String key, final String... tokens) throws SikevaDBException;
}
