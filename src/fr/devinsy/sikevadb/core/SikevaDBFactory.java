/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.core;

import java.io.File;

import fr.devinsy.sikevadb.filetree.FileTreeSikevaDB;
import fr.devinsy.sikevadb.sql.SQLSikevaDB;

/**
 * A factory for creating SikevaDB objects.
 * 
 * @author Christian Pierre MOMON
 */
public class SikevaDBFactory
{
	/**
	 * Gets a SikevaDB instance from its database home directory.
	 * 
	 * @param path
	 *            the path of the database home directory
	 * 
	 * @return a SikevaDB instance
	 * 
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public static SikevaDB get(final File path) throws SikevaDBException
	{
		SikevaDB result;

		result = new FileTreeSikevaDB(path, null, null);

		//
		return result;
	}

	/**
	 * Gets a SikevaDB instance from a context.
	 * 
	 * @param contextName
	 *            the context name
	 * 
	 * @return a SikevaDB instance
	 * 
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public static SikevaDB get(final String contextName)
	{
		SikevaDB result;

		result = new SQLSikevaDB(contextName);

		//
		return result;
	}

	/**
	 * Gets a SikevaDB instance from dabase parameters.
	 * 
	 * @param driverClassName
	 *            the driver class name
	 * @param url
	 *            the url
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 * 
	 * @return the SikevaDB
	 */
	public static SikevaDB get(final String driverClassName, final String url, final String login, final String password)
	{
		SikevaDB result;

		result = new SQLSikevaDB(driverClassName, url, login, password);

		//
		return result;
	}
}
