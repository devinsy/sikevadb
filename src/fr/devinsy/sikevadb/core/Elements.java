/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.core;

import java.util.ArrayList;

/**
 * The Class Elements.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Elements extends ArrayList<Element>
{
	private static final long serialVersionUID = 6298920093179395791L;

	/**
	 * Instantiates a new elements.
	 */
	public Elements()
	{
		super();
	}

	/**
	 * Instantiates a new elements.
	 * 
	 * @param initialCapacity
	 *            the initial capacity
	 */
	public Elements(final int initialCapacity)
	{
		super(initialCapacity);
	}
}
