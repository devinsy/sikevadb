/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.core;

import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;

/**
 * The Class Element.
 * 
 * @author Christian Pierre MOMON
 */
public class Element
{
	public static final long NO_ID = -1;

	private long id;
	private DateTime creationDate;
	private DateTime editionDate;
	/* archiveDate field is reserved for journalizer. */
	private DateTime archiveDate;
	private String key;
	private String subkey;
	private long size;
	private String digest;
	private String value;

	/**
	 * Instantiates a new element.
	 */
	public Element()
	{
		this.id = NO_ID;
	}

	/**
	 * Instantiates a new element.
	 * 
	 * @param key
	 *            the key
	 * @param subkey
	 *            the subkey
	 * @param value
	 *            the value
	 */
	public Element(final String key, final String subkey, final String value)
	{
		update(value);
		this.archiveDate = null;
		this.key = key;
		this.subkey = subkey;
	}

	/**
	 * Archive.
	 */
	public void archive()
	{
		this.archiveDate = DateTime.now();
	}

	/**
	 * Gets the archive date.
	 * 
	 * @return the archive date
	 */
	public DateTime getArchiveDate()
	{
		return this.archiveDate;
	}

	/**
	 * Gets the creation date.
	 * 
	 * @return the creation date
	 */
	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	/**
	 * Gets the digest.
	 * 
	 * @return the digest
	 */
	public String getDigest()
	{
		return this.digest;
	}

	/**
	 * Gets the edition date.
	 * 
	 * @return the edition date
	 */
	public DateTime getEditionDate()
	{
		return this.editionDate;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public long getId()
	{
		return this.id;
	}

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 */
	public String getKey()
	{
		return this.key;
	}

	/**
	 * Gets the size.
	 * 
	 * @return the size
	 */
	public long getSize()
	{
		return this.size;
	}

	/**
	 * Gets the subkey.
	 * 
	 * @return the subkey
	 */
	public String getSubkey()
	{
		return this.subkey;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue()
	{
		return this.value;
	}

	/**
	 * Sets the archive date.
	 * 
	 * @param date
	 *            the new archive date
	 */
	public void setArchiveDate(final DateTime date)
	{
		this.archiveDate = date;
	}

	/**
	 * Sets the creation date.
	 * 
	 * @param date
	 *            the new creation date
	 */
	public void setCreationDate(final DateTime date)
	{
		this.creationDate = date;
	}

	/**
	 * Sets the digest.
	 * 
	 * @param digest
	 *            the new digest
	 */
	public void setDigest(final String digest)
	{
		this.digest = digest;
	}

	/**
	 * Sets the edition date.
	 * 
	 * @param date
	 *            the new edition date
	 */
	public void setEditionDate(final DateTime date)
	{
		this.editionDate = date;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(final long id)
	{
		this.id = id;
	}

	/**
	 * Sets the key.
	 * 
	 * @param key
	 *            the new key
	 */
	public void setKey(final String key)
	{
		this.key = key;
	}

	/**
	 * Sets the size.
	 * 
	 * @param size
	 *            the new size
	 */
	public void setSize(final long size)
	{
		this.size = size;
	}

	/**
	 * Sets the subkey.
	 * 
	 * @param subkey
	 *            the new subkey
	 */
	public void setSubkey(final String subkey)
	{
		this.subkey = subkey;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(final String value)
	{
		this.value = value;
	}

	/**
	 * Update.
	 * 
	 * @param value
	 *            the value
	 */
	public void update(final String value)
	{
		//
		setValue(value);

		//
		this.editionDate = DateTime.now();

		//
		if (value == null)
		{
			this.size = 0;
			this.digest = DigestUtils.md5Hex("");
		}
		else
		{
			this.size = this.value.length();
			this.digest = DigestUtils.md5Hex(this.value);
		}

		//
		if (this.creationDate == null)
		{
			this.creationDate = this.editionDate;
		}
	}
}
