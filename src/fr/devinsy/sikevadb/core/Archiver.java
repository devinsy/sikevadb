/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.core;

/**
 * The Interface Archiver.
 * 
 * @author Christian Pierre MOMON
 */
public interface Archiver
{
	public enum Status
	{
		ACTIVATED,
		SUSPENDED
	}

	void activate();

	void archive(Element element) throws SikevaDBException;

	void clear() throws SikevaDBException;

	void close();

	boolean isActivated();

	boolean isSuspended();

	void suspend();
}
