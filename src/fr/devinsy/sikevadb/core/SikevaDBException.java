/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.core;

/**
 * The Class SikevaDBException.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class SikevaDBException extends Exception
{
	private static final long serialVersionUID = 8323299610751708972L;

	/**
	 * Instantiates a new SikevaDB exception.
	 */
	public SikevaDBException()
	{
		super();
	}

	/**
	 * Instantiates a new SikevaDB exception.
	 * 
	 * @param message
	 *            the message
	 */
	public SikevaDBException(final String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new SikevaDB exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public SikevaDBException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Instantiates a new SikevaDB exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public SikevaDBException(final Throwable cause)
	{
		super(cause);
	}
}
