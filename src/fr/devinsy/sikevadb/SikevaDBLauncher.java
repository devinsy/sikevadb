/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb;

import java.io.File;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.sikevadb.cli.SikevaDBCLI;
import fr.devinsy.sikevadb.demo.SikevaDBDemo;

/**
 * The Class SikevaDBLauncher.
 */
public final class SikevaDBLauncher
{
	private static Logger logger = LoggerFactory.getLogger(SikevaDBLauncher.class);

	/**
	 * Instantiates a new SikevaDB launcher.
	 */
	private SikevaDBLauncher()
	{
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		// Configure log.
		File loggerConfig = new File("log4j.properties");
		if (loggerConfig.exists())
		{
			PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
			logger.info("Dedicated log configuration done.");
			logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
		}
		else
		{
			BasicConfigurator.configure();
			logger.info("Basic log configuration done.");
			logger.info("Configuration file was not found in [{}].", loggerConfig.getAbsoluteFile());
		}

		// Run.
		if (args.length == 0)
		{
			// TODO
			// SikevaGUI.run();
		}
		else if (ArrayUtils.contains(args, "-demo"))
		{
			SikevaDBDemo.run(args);
		}
		else
		{
			SikevaDBCLI.run(args);
		}
	}
}
