-- Specific file for HSQLDB due difference between MySQL yntax and HSQLDB syntax.
-- Warning: there is a convention about the file name.

DROP TABLE IF EXISTS sikevadb_elements;
CREATE TABLE IF NOT EXISTS sikevadb_elements (
  `ID` INTEGER NOT NULL AUTO_INCREMENT,
  `TOPKEY` varchar(255) NOT NULL,
  `SUBKEY` varchar(255) DEFAULT NULL,
  `VALUE` longtext,
  `SIZE` INTEGER NOT NULL DEFAULT '0',
  `DIGEST` varchar(255) NOT NULL,
  `CREATION_DATE` datetime NOT NULL,
  `EDITION_DATE` datetime NOT NULL,
  `ARCHIVE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
);
