/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.sql;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.strings.StringList;

/**
 * The Class SQLSikevaDBTools.
 * 
 * @author Christian Pierre MOMON
 */
public class SQLSikevaDBTools
{
	private static final Logger logger = LoggerFactory.getLogger(SQLSikevaDBTools.class);

	/**
	 * Load SQL script.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList loadSQLScript(final URL source) throws IOException
	{
		StringList result;

		String script = IOUtils.toString(source);

		result = splitSQLCommands(script);

		//
		return result;
	}

	/**
	 * Split SQL commands.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList splitSQLCommands(final String source) throws IOException
	{
		StringList result;

		//
		result = new StringList();

		//
		if (source != null)
		{
			//
			BufferedReader in = new BufferedReader(new StringReader(source));

			boolean ended = false;
			StringList sql = new StringList();
			while (!ended)
			{
				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					if ((StringUtils.isNotBlank(line)) && (!line.startsWith("--")))
					{
						if (line.endsWith(";"))
						{
							sql.append(line.substring(0, line.length() - 1));

							result.add(sql.toString());

							sql.clear();
						}
						else
						{
							sql.append(line).append(' ');
						}
					}
				}
			}

			//
			in.close();
		}

		//
		return result;
	}

	/**
	 * To date time.
	 * 
	 * @param source
	 *            the source
	 * @return the date time
	 */
	public static DateTime toDateTime(final java.sql.Timestamp source)
	{
		DateTime result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new DateTime(source.getTime());
		}

		//
		return result;
	}

	/**
	 * To timestamp.
	 * 
	 * @param source
	 *            the source
	 * @return the java.sql. timestamp
	 */
	public static java.sql.Timestamp toTimestamp(final Date source)
	{
		java.sql.Timestamp result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new java.sql.Timestamp(source.getTime());
		}

		//
		return result;
	}

	/**
	 * To timestamp.
	 * 
	 * @param source
	 *            the source
	 * @return the java.sql. timestamp
	 */
	public static java.sql.Timestamp toTimestamp(final DateTime source)
	{
		java.sql.Timestamp result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new java.sql.Timestamp(source.getMillis());
		}

		//
		return result;
	}
}
