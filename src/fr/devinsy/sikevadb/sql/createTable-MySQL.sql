-- Warning: there is a convention about the file name.

-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Structure de la table `elements`
--

DROP TABLE IF EXISTS `sikevadb_elements`;
CREATE TABLE IF NOT EXISTS `sikevadb_elements` (
  `ID` INTEGER NOT NULL AUTO_INCREMENT,
  `TOPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SUBKEY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VALUE` longtext COLLATE utf8_unicode_ci,
  `SIZE` INTEGER NOT NULL DEFAULT '0',
  `DIGEST` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CREATION_DATE` datetime NOT NULL,
  `EDITION_DATE` datetime NOT NULL,
  `ARCHIVE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `XKEYINDEX` (`TOPKEY`,`SUBKEY`),
  KEY `TOPKEYINDEX` (`TOPKEY`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
