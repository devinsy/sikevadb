/*
 * Copyright (C) 2013-2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.sql;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.sikevadb.core.Archiver;
import fr.devinsy.sikevadb.core.ClosedDatabaseException;
import fr.devinsy.sikevadb.core.Element;
import fr.devinsy.sikevadb.core.Elements;
import fr.devinsy.sikevadb.core.OpenedDatabaseException;
import fr.devinsy.sikevadb.core.SikevaDB;
import fr.devinsy.sikevadb.core.SikevaDBException;
import fr.devinsy.util.strings.StringList;

/**
 * The Class SQLSikevaDB.
 * 
 * @author Christian Pierre MOMON
 */

public class SQLSikevaDB implements SikevaDB
{
	public enum Status
	{
		OPENED,
		CLOSED
	};

	private static final Logger logger = LoggerFactory.getLogger(SQLSikevaDB.class);;

	private Status status;
	private String driverClassname;
	private String url;
	private String login;
	private String password;

	private String contextName;

	private Connection singleConnection;
	private DataSource dataSource;
	private Archiver archiver;

	/**
	 * Instantiates a new SQL SikevaDB.
	 * 
	 * @param contextName
	 *            the context name
	 */
	public SQLSikevaDB(final String contextName)
	{
		this.status = Status.CLOSED;
		this.contextName = contextName;
		this.archiver = null;
	}

	/**
	 * This method opens a database session.
	 * 
	 * @param driverClassName
	 *            the driver class name
	 * @param url
	 *            the url
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 */
	public SQLSikevaDB(final String driverClassName, final String url, final String login, final String password)
	{
		if (StringUtils.isBlank(driverClassName))
		{
			throw new IllegalArgumentException("driverClassName is null.");
		}
		else if (StringUtils.isBlank(url))
		{
			throw new IllegalArgumentException("url is null.");
		}
		else if (StringUtils.isBlank(login))
		{
			throw new IllegalArgumentException("login is null.");
		}
		else if (password == null)
		{
			throw new IllegalArgumentException("password is null.");
		}
		else
		{
			this.status = Status.CLOSED;
			this.driverClassname = driverClassName;
			this.url = url;
			this.login = login;
			this.password = password;
			this.dataSource = null;
			this.archiver = null;
		}
	}

	/**
	 * Activate archiver.
	 * 
	 * @throws ClosedDatabaseException
	 */
	public void activateArchiver() throws ClosedDatabaseException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			this.archiver.activate();
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#archiver()
	 */
	@Override
	public Archiver archiver() throws SikevaDBException
	{
		Archiver result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = this.archiver;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#clear()
	 */
	@Override
	public void clear() throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("DELETE FROM sikevadb_elements");

				statement.executeUpdate();
			}
			catch (SQLException exception)
			{
				logger.error("SQL Error getting connection.", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#close()
	 */
	@Override
	public void close()
	{
		//
		if (this.singleConnection != null)
		{
			try
			{
				this.singleConnection.close();
			}
			catch (SQLException exception)
			{
				logger.error("Error closing database.", exception);
			}
			finally
			{
				this.singleConnection = null;
			}
		}

		//
		this.dataSource = null;

		this.status = Status.CLOSED;
	}

	/**
	 * Close quietly.
	 * 
	 * @param connection
	 *            the connection
	 * @param statement
	 *            the statement
	 * @param resultSet
	 *            the result set
	 */
	private void closeQuietly(final Connection connection, final Statement statement, final ResultSet resultSet)
	{
		//
		if ((connection != null) && (connection != this.singleConnection))
		{
			try
			{
				connection.close();
			}
			catch (SQLException exception)
			{
				exception.printStackTrace();
			}
		}

		//
		if (statement != null)
		{
			try
			{
				statement.close();
			}
			catch (SQLException exception)
			{
				exception.printStackTrace();
			}
		}

		//
		if (resultSet != null)
		{
			try
			{
				resultSet.close();
			}
			catch (SQLException exception)
			{
				exception.printStackTrace();
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#countOfElements()
	 */
	@Override
	public long countOfElements() throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			Connection connection = null;
			Statement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.createStatement();
				resultSet = statement.executeQuery("SELECT count(*) FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL");

				resultSet.next();
				result = resultSet.getLong(1);
			}
			catch (SQLException exception)
			{
				logger.error("Error counting elements.", exception);
				throw new SikevaDBException("Error counting elements", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#countOfElements(java.lang.String)
	 */
	@Override
	public long countOfElements(final String key) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT count(*) FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY IS NULL");
				statement.setString(1, key);
				resultSet = statement.executeQuery();

				resultSet.next();
				result = resultSet.getLong(1);
			}
			catch (SQLException exception)
			{
				logger.error("Error counting elements.", exception);
				throw new SikevaDBException("Error counting elements", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#countOfElements(java.lang.String, long)
	 */
	@Override
	public long countOfElements(final String key, final long subkey) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = countOfElements(key, String.valueOf(subkey));
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#countOfElements(java.lang.String, java.lang.String)
	 */
	@Override
	public long countOfElements(final String key, final String subkey) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else if (subkey == null)
		{
			throw new IllegalArgumentException("Subkey is null.");
		}
		else
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT count(*) FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY=?");
				statement.setString(1, key);
				statement.setString(2, subkey);
				resultSet = statement.executeQuery();

				resultSet.next();
				result = resultSet.getLong(1);
			}
			catch (SQLException exception)
			{
				logger.error("Error counting elements.", exception);
				throw new SikevaDBException("Error counting elements", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#create()
	 */
	@Override
	public void create() throws SikevaDBException
	{
		if (isOpened())
		{
			throw new OpenedDatabaseException();
		}
		else
		{
			//
			open();

			//
			Connection connection = null;
			Statement statement = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(true);

				System.out.println("driver name =" + connection.getMetaData().getDriverName());
				System.out.println("driver version =" + connection.getMetaData().getDriverVersion());
				System.out.println("driver database name =" + connection.getMetaData().getDatabaseProductName());
				System.out.println("driver database version =" + connection.getMetaData().getDatabaseProductVersion());

				//
				StringList sqlCommands;
				String databaseProductName = connection.getMetaData().getDatabaseProductName().split(" ")[0];
				logger.debug("[datatbaseProductName={}]", databaseProductName);
				sqlCommands = SQLSikevaDBTools.loadSQLScript(SQLSikevaDB.class.getResource("/fr/devinsy/sikevadb/sql/createTable-" + databaseProductName + ".sql"));

				if (sqlCommands == null)
				{
					throw new FileNotFoundException("SQL script creation not found for [" + databaseProductName + "].");
				}
				else
				{
					//
					for (String sql : sqlCommands)
					{
						System.out.println("sql=[" + sql + "]");

						statement = connection.createStatement();
						statement.execute(sql);
					}

					System.out.println("============================== APRÈS");

					statement = connection.createStatement();
					statement.execute("delete from sikevadb_elements");

					System.out.println("============================== APRÈS2");
				}

				//
				close();
			}
			catch (SQLException exception)
			{
				logger.error("Error creating schema.", exception);
				throw new SikevaDBException("Error creating schema.", exception);
			}
			catch (IOException exception)
			{
				logger.error("Error IO creating schema.", exception);
				throw new SikevaDBException("Error IO creating schema.", exception);
			}
			finally
			{
				closeQuietly(connection, statement, null);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#delete(java.lang.String)
	 */
	@Override
	public void delete(final String key) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else
		{
			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("DELETE FROM sikevadb_elements WHERE TOPKEY=? AND SUBKEY IS NULL AND ARCHIVE_DATE IS NULL");

				statement.setString(1, key);

				int rowCount = statement.executeUpdate();

				if (rowCount == 0)
				{
					logger.warn("Delete action without existing target [key=" + key + "]");
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error deleting element.", exception);
				try
				{
					connection.rollback();
				}
				catch (SQLException exception1)
				{
					logger.error("Rollback failed.", exception);
				}
				throw new SikevaDBException("Error deleting element", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#delete(java.lang.String, long)
	 */
	@Override
	public void delete(final String key, final long subkey) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			delete(key, String.valueOf(subkey));
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#delete(java.lang.String, java.lang.String)
	 */
	@Override
	public void delete(final String key, final String subkey) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else if (subkey == null)
		{
			throw new IllegalArgumentException("Subkey is null.");
		}
		else
		{
			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				statement = connection.prepareStatement("DELETE FROM sikevadb_elements WHERE TOPKEY=? AND SUBKEY=? AND ARCHIVE_DATE IS NULL");

				statement.setString(1, key);
				statement.setString(2, subkey);

				int rowCount = statement.executeUpdate();

				if (rowCount == 0)
				{
					logger.warn("Delete action without existing target [key=" + key + "][subkey=" + subkey + "]");
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error deleting subkey.", exception);
				throw new SikevaDBException("Error deleting subkey", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#deleteMany(java.lang.String, java.lang.String[])
	 */
	@Override
	public void deleteMany(final String key, final String... subkeys) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else if ((subkeys != null) && (subkeys.length > 0))
		{
			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();

				//
				String questionMarks = new StringList(subkeys.length).append('?').repeatLast(subkeys.length - 1).toStringWithCommas();
				statement = connection.prepareStatement("DELETE FROM sikevadb_elements WHERE TOPKEY=? AND SUBKEY IN (" + questionMarks + ") AND ARCHIVE_DATE IS NULL");
				//
				statement.setString(1, key);

				//
				for (int index = 0; index < subkeys.length; index++)
				{
					statement.setString(2 + index, subkeys[index]);
				}

				//
				int rowCount = statement.executeUpdate();
				if (rowCount == 0)
				{
					logger.warn("Delete action without existing target [key=" + key + "][subkeys=" + new StringList(subkeys).toStringWithCommas() + "]");
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error deleting subkeys.", exception);
				throw new SikevaDBException("Error deleting subkeys", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#destroy()
	 */
	@Override
	public void destroy() throws SikevaDBException
	{
		if (isOpened())
		{
			throw new OpenedDatabaseException();
		}
		else
		{
			// TODO
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#exists()
	 */
	@Override
	public boolean exists() throws SikevaDBException
	{
		boolean result;

		// TODO
		result = false;

		//
		return result;
	}

	/**
	 * Exists.
	 * 
	 * @param element
	 *            the element
	 * @return true, if successful
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public boolean exists(final Element element) throws SikevaDBException
	{
		boolean result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			if (element == null)
			{
				result = false;
			}
			else if (getElement(element.getId()) == null)
			{
				result = false;
			}
			else
			{
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * Gets the connection.
	 * 
	 * @return the connection
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public Connection getConnection() throws SikevaDBException
	{
		Connection result;

		if (this.singleConnection != null)
		{
			result = this.singleConnection;
		}
		else if (this.dataSource != null)
		{
			try
			{
				result = this.dataSource.getConnection();
			}
			catch (SQLException exception)
			{
				logger.error("Error counting elements.", exception);
				throw new SikevaDBException("Error counting elements", exception);
			}
		}
		else
		{
			throw new IllegalArgumentException("Connection not initialized.");
		}

		//
		return result;
	}

	/**
	 * Gets the context name.
	 * 
	 * @return the context name
	 */
	public String getContextName()
	{
		String result;

		result = this.contextName;

		//
		return result;
	}

	/**
	 * Gets the driver classname.
	 * 
	 * @return the driver classname
	 */
	public String getDriverClassname()
	{
		String result;

		result = this.driverClassname;

		//
		return result;
	}

	/**
	 * Gets the element.
	 * 
	 * @param id
	 *            the id
	 * @return the element
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public Element getElement(final long id) throws SikevaDBException
	{
		Element result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (id == Element.NO_ID)
		{
			result = null;
		}
		else
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT ID,TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE FROM sikevadb_elements WHERE ID=?");
				statement.setLong(1, id);
				resultSet = statement.executeQuery();

				//
				if (resultSet.next())
				{
					//
					result = new Element();

					result.setId(resultSet.getLong(1));
					result.setKey(resultSet.getString(2));
					result.setSubkey(resultSet.getString(3));
					result.setValue(resultSet.getString(4));
					result.setSize(resultSet.getLong(5));
					result.setDigest(resultSet.getString(6));
					result.setCreationDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(7)));
					result.setEditionDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(8)));
					result.setArchiveDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(9)));

					//
					if (resultSet.next())
					{
						throw new SikevaDBException("More than only once result [id=" + id + "].");
					}
				}
				else
				{
					result = null;
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error getting element.", exception);
				throw new SikevaDBException("Error getting element", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElement(java.lang.String)
	 */
	@Override
	public Element getElement(final String key) throws SikevaDBException
	{
		Element result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection
						.prepareStatement("SELECT ID,TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY IS NULL");
				statement.setString(1, key);
				resultSet = statement.executeQuery();

				//
				if (resultSet.next())
				{
					//
					result = new Element();

					result.setId(resultSet.getLong(1));
					result.setKey(resultSet.getString(2));
					result.setSubkey(resultSet.getString(3));
					result.setValue(resultSet.getString(4));
					result.setSize(resultSet.getLong(5));
					result.setDigest(resultSet.getString(6));
					result.setCreationDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(7)));
					result.setEditionDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(8)));
					result.setArchiveDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(9)));

					//
					if (resultSet.next())
					{
						throw new SikevaDBException("More than only once result.");
					}
				}
				else
				{
					result = null;
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error getting element.", exception);
				throw new SikevaDBException("Error getting element", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElement(java.lang.String, long)
	 */
	@Override
	public Element getElement(final String key, final long subkey) throws SikevaDBException
	{
		Element result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = getElement(key, String.valueOf(subkey));
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElement(java.lang.String, java.lang.String)
	 */
	@Override
	public Element getElement(final String key, final String subkey) throws SikevaDBException
	{
		Element result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else if (subkey == null)
		{
			throw new IllegalArgumentException("Subkey is null.");
		}
		else
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection
						.prepareStatement("SELECT ID,TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY=?");
				statement.setString(1, key);
				statement.setString(2, subkey);
				resultSet = statement.executeQuery();

				//
				if (resultSet.next())
				{
					//
					result = new Element();

					result.setId(resultSet.getLong(1));
					result.setKey(resultSet.getString(2));
					result.setSubkey(resultSet.getString(3));
					result.setValue(resultSet.getString(4));
					result.setSize(resultSet.getLong(5));
					result.setDigest(resultSet.getString(6));
					result.setCreationDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(7)));
					result.setEditionDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(8)));
					result.setArchiveDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(9)));

					//
					if (resultSet.next())
					{
						throw new SikevaDBException("More than only once result.");
					}
				}
				else
				{
					result = null;
				}

			}
			catch (SQLException exception)
			{
				logger.error("Error getting element.", exception);
				throw new SikevaDBException("Error getting element", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElements()
	 */
	@Override
	public Elements getElements() throws SikevaDBException
	{
		Elements result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			//
			result = new Elements((int) countOfElements());

			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection
						.prepareStatement("SELECT ID,TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL ORDER BY TOPKEY,SUBKEY ASC");
				resultSet = statement.executeQuery();

				while (resultSet.next())
				{
					//
					Element element = new Element();

					element.setId(resultSet.getLong(1));
					element.setKey(resultSet.getString(2));
					element.setSubkey(resultSet.getString(3));
					element.setValue(resultSet.getString(4));
					element.setSize(resultSet.getLong(5));
					element.setDigest(resultSet.getString(6));
					element.setCreationDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(7)));
					element.setEditionDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(8)));
					element.setArchiveDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(9)));

					//
					result.add(element);
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error getting element.", exception);
				throw new SikevaDBException("Error getting element", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElements(java.lang.String)
	 */
	@Override
	public Elements getElements(final String key) throws SikevaDBException
	{
		Elements result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else
		{
			//
			result = new Elements((int) countOfElements(key));

			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection
						.prepareStatement("SELECT ID,TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY IS NULL ORDER BY CREATION_DATE ASC");
				statement.setString(1, key);
				resultSet = statement.executeQuery();

				while (resultSet.next())
				{
					//
					Element element = new Element();

					element.setId(resultSet.getLong(1));
					element.setKey(resultSet.getString(2));
					element.setSubkey(resultSet.getString(3));
					element.setValue(resultSet.getString(4));
					element.setSize(resultSet.getLong(5));
					element.setDigest(resultSet.getString(6));
					element.setCreationDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(7)));
					element.setEditionDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(8)));
					element.setArchiveDate(SQLSikevaDBTools.toDateTime(resultSet.getTimestamp(9)));

					//
					result.add(element);
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error getting element.", exception);
				throw new SikevaDBException("Error getting element", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the login.
	 * 
	 * @return the login
	 */
	public String getLogin()
	{
		String result;

		result = this.login;

		//
		return result;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword()
	{
		String result;

		result = this.password;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getSubkeys(java.lang.String)
	 */
	@Override
	public StringList getSubkeys(final String key) throws SikevaDBException
	{
		StringList result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else
		{
			//
			result = new StringList();

			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection
						.prepareStatement("SELECT DISTINCT SUBKEY,CREATION_DATE FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY IS NOT NULL ORDER BY CREATION_DATE ASC");
				statement.setString(1, key);
				resultSet = statement.executeQuery();

				while (resultSet.next())
				{
					result.add(resultSet.getString(1));
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error getting subkeys.", exception);
				throw new SikevaDBException("Error getting subkeys", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getKeys()
	 */
	@Override
	public StringList getTopKeys() throws SikevaDBException
	{
		StringList result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			//
			result = new StringList();

			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT DISTINCT TOPKEY FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL");
				resultSet = statement.executeQuery();

				while (resultSet.next())
				{
					result.add(resultSet.getString(1));
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error getting keys.", exception);
				throw new SikevaDBException("Error getting keys", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	public String getUrl()
	{
		String result;

		result = this.url;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getValue(java.lang.String)
	 */
	@Override
	public String getValue(final String key) throws SikevaDBException
	{
		String result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT VALUE FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY IS NULL");
				statement.setString(1, key);
				resultSet = statement.executeQuery();

				//
				if (resultSet.next())
				{
					result = resultSet.getString(1);

					if (resultSet.next())
					{
						throw new SikevaDBException("More than only once result.");
					}
				}
				else
				{
					result = null;
				}

			}
			catch (SQLException exception)
			{
				logger.error("Error getting value.", exception);
				throw new SikevaDBException("Error getting value", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getValue(java.lang.String, long)
	 */
	@Override
	public String getValue(final String key, final long subkey) throws SikevaDBException
	{
		String result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = getValue(key, String.valueOf(subkey));
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getValue(java.lang.String, java.lang.String)
	 */
	@Override
	public String getValue(final String key, final String subkey) throws SikevaDBException
	{
		String result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else if (subkey == null)
		{
			throw new IllegalArgumentException("Subkey is null.");
		}
		else
		{
			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT VALUE FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY=?");
				statement.setString(1, key);
				statement.setString(2, subkey);
				resultSet = statement.executeQuery();

				//
				if (resultSet.next())
				{
					result = resultSet.getString(1);

					if (resultSet.next())
					{
						throw new SikevaDBException("More than only once result.");
					}
				}
				else
				{
					result = null;
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error getting value.", exception);
				throw new SikevaDBException("Error getting value", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getValues(java.lang.String)
	 */
	@Override
	public StringList getValues(final String key) throws SikevaDBException
	{
		StringList result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			//
			result = new StringList((int) countOfElements(key));

			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT VALUE,CREATION_DATE FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY IS NOT NULL ORDER BY CREATION_DATE ASC");
				statement.setString(1, key);
				resultSet = statement.executeQuery();

				while (resultSet.next())
				{
					result.add(resultSet.getString(1));
				}
			}
			catch (SQLException exception)
			{
				logger.error("Error getting values.", exception);
				throw new SikevaDBException("Error getting values", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isArchiveActivated()
	 */
	@Override
	public boolean isArchiveActivated()
	{
		boolean result;

		if (isOpened())
		{
			result = this.archiver.isActivated();
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isArchiveSuspended()
	 */
	@Override
	public boolean isArchiveSuspended()
	{
		boolean result;

		result = !isArchiveActivated();

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isClosed()
	 */
	@Override
	public boolean isClosed()
	{
		boolean result;

		if (this.status == Status.CLOSED)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isOpened()
	 */
	@Override
	public boolean isOpened()
	{
		boolean result;

		result = !isClosed();

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#memorySize()
	 */
	@Override
	public long memorySize() throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT SUM(SIZE) FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL");
				resultSet = statement.executeQuery();

				resultSet.next();
				result = resultSet.getLong(1);
			}
			catch (SQLException exception)
			{
				logger.error("Error computing memory size.", exception);
				throw new SikevaDBException("Error computing memory size", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#memorySize(java.lang.String)
	 */
	@Override
	public long memorySize(final String key) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT SUM(SIZE) FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=?");
				statement.setString(1, key);
				resultSet = statement.executeQuery();

				resultSet.next();
				result = resultSet.getLong(1);
			}
			catch (SQLException exception)
			{
				logger.error("Error computing key memory size.", exception);
				throw new SikevaDBException("Error computing key memory size.", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#memorySize(java.lang.String, long)
	 */
	@Override
	public long memorySize(final String key, final long subkey) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = memorySize(key, String.valueOf(subkey));
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#memorySize(java.lang.String, java.lang.String)
	 */
	@Override
	public long memorySize(final String key, final String subkey) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else if (subkey == null)
		{
			throw new IllegalArgumentException("Subkey is null.");
		}
		else
		{
			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("SELECT SUM(SIZE) FROM sikevadb_elements WHERE ARCHIVE_DATE IS NULL AND TOPKEY=? AND SUBKEY=?");
				statement.setString(1, key);
				statement.setString(2, subkey);
				resultSet = statement.executeQuery();

				resultSet.next();
				result = resultSet.getLong(1);
			}
			catch (SQLException exception)
			{
				logger.error("Error computing subkey memory size.", exception);
				throw new SikevaDBException("Error computing subkey memory size", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#open()
	 */
	@Override
	public void open() throws SikevaDBException
	{
		try
		{
			//
			close();

			//
			if (this.url != null)
			{
				Class.forName(this.driverClassname).newInstance();
				this.singleConnection = DriverManager.getConnection(this.url, this.login, this.password);
				logger.info("Single connection opened with [{}].", this.url);
				this.status = Status.OPENED;
				this.archiver = new SQLArchiver(this);
			}
			else if (this.contextName != null)
			{
				Context initialContext = new InitialContext();
				Context environmentContext = (Context) initialContext.lookup("java:comp/env");
				this.dataSource = (DataSource) environmentContext.lookup(this.contextName);
				logger.info("Database {} opened.", this.contextName);
				this.status = Status.OPENED;
				this.archiver = new SQLArchiver(this);
			}
			else
			{
				throw new IllegalArgumentException("Undefined source.");
			}
		}
		catch (InstantiationException exception)
		{
			logger.error("Error opening database.", exception);
			throw new SikevaDBException("Error opening database.");
		}
		catch (IllegalAccessException exception)
		{
			logger.error("Error opening database.", exception);
			throw new SikevaDBException("Error opening database.");
		}
		catch (ClassNotFoundException exception)
		{
			logger.error("Error opening database.", exception);
			throw new SikevaDBException("Error opening database.");
		}
		catch (SQLException exception)
		{
			logger.error("Error opening database.", exception);
			throw new SikevaDBException("Error opening database.");
		}
		catch (NamingException exception)
		{
			logger.error("Error opening database.", exception);
			throw new SikevaDBException("Error opening database.");
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#put(fr.devinsy.sikevadb.core.Element)
	 */
	@Override
	public void put(final Element element) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (element == null)
		{
			throw new IllegalArgumentException("element is null.");
		}
		else
		{
			if (exists(element))
			{
				//
				Connection connection = null;
				PreparedStatement statement = null;
				ResultSet resultSet = null;
				try
				{
					//
					connection = getConnection();
					connection.setAutoCommit(true);

					// Archive existing element.
					statement = connection.prepareStatement("UPDATE sikevadb_elements SET TOPKEY=?,SUBKEY=?,VALUE=?,SIZE=?,DIGEST=?,CREATION_DATE=?,EDITION_DATE=?,ARCHIVE_DATE=? WHERE ID=?");

					statement.setString(1, element.getKey());
					statement.setString(2, element.getSubkey());
					statement.setString(3, element.getValue());
					statement.setLong(4, element.getSize());
					statement.setString(5, element.getDigest());
					statement.setTimestamp(6, SQLSikevaDBTools.toTimestamp(element.getCreationDate()));
					statement.setTimestamp(7, SQLSikevaDBTools.toTimestamp(element.getEditionDate()));
					statement.setTimestamp(8, SQLSikevaDBTools.toTimestamp(element.getArchiveDate()));
					statement.setLong(9, element.getId());

					statement.executeUpdate();
				}
				catch (SQLException exception)
				{
					logger.error("Error putting element.", exception);
					throw new SikevaDBException("Error putting element", exception);
				}
				finally
				{
					closeQuietly(connection, statement, resultSet);
				}
			}
			else if (element.getId() == Element.NO_ID)
			{
				//
				Connection connection = null;
				PreparedStatement statement = null;
				ResultSet resultSet = null;
				try
				{
					connection = getConnection();
					connection.setAutoCommit(true);
					statement = connection.prepareStatement("INSERT INTO sikevadb_elements (TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

					statement.setString(1, element.getKey());
					statement.setString(2, element.getSubkey());
					statement.setString(3, element.getValue());
					statement.setLong(4, element.getSize());
					statement.setString(5, element.getDigest());
					statement.setTimestamp(6, SQLSikevaDBTools.toTimestamp(element.getCreationDate()));
					statement.setTimestamp(7, SQLSikevaDBTools.toTimestamp(element.getEditionDate()));
					statement.setTimestamp(8, SQLSikevaDBTools.toTimestamp(element.getArchiveDate()));

					statement.executeUpdate();
				}
				catch (SQLException exception)
				{
					logger.error("Error putting element.", exception);
					throw new SikevaDBException("Error putting element", exception);
				}
				finally
				{
					closeQuietly(connection, statement, resultSet);
				}
			}
			else
			{
				//
				Connection connection = null;
				PreparedStatement statement = null;
				ResultSet resultSet = null;
				try
				{
					connection = getConnection();
					connection.setAutoCommit(true);
					statement = connection
							.prepareStatement("INSERT INTO sikevadb_elements (ID,TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE) VALUES(?,?, ?, ?, ?, ?, ?, ?, ?)");

					statement.setLong(1, element.getId());
					statement.setString(2, element.getKey());
					statement.setString(3, element.getSubkey());
					statement.setString(4, element.getValue());
					statement.setLong(5, element.getSize());
					statement.setString(6, element.getDigest());
					statement.setTimestamp(7, SQLSikevaDBTools.toTimestamp(element.getCreationDate()));
					statement.setTimestamp(8, SQLSikevaDBTools.toTimestamp(element.getEditionDate()));
					statement.setTimestamp(9, SQLSikevaDBTools.toTimestamp(element.getArchiveDate()));

					statement.executeUpdate();
				}
				catch (SQLException exception)
				{
					logger.error("Error putting element.", exception);
					throw new SikevaDBException("Error putting element", exception);
				}
				finally
				{
					closeQuietly(connection, statement, resultSet);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#put(java.lang.String, long, java.lang.String)
	 */
	@Override
	public void put(final String key, final long subkey, final String value) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			put(key, String.valueOf(subkey), String.valueOf(value));
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#put(java.lang.String, java.lang.String)
	 */
	@Override
	public void put(final String key, final String value) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			//
			Element element = getElement(key);

			//
			if (element == null)
			{
				element = new Element();
				element.setKey(key);
				element.update(value);
				element.setArchiveDate(null);
			}
			else
			{
				element.update(value);
			}

			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(false);

				// Archive existing element.
				if (element.getId() != Element.NO_ID)
				{
					statement = connection.prepareStatement("UPDATE sikevadb_elements SET ARCHIVE_DATE=? WHERE TOPKEY=? AND SUBKEY IS NULL AND ARCHIVE_DATE IS NULL");

					statement.setTimestamp(1, SQLSikevaDBTools.toTimestamp(element.getEditionDate()));
					statement.setString(2, element.getKey());

					statement.executeUpdate();
					statement.clearParameters();
					statement.close();
				}

				// Insert new version of the element.
				statement = connection.prepareStatement("INSERT INTO sikevadb_elements (TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

				statement.setString(1, element.getKey());
				statement.setString(2, element.getSubkey());
				statement.setString(3, element.getValue());
				statement.setLong(4, element.getSize());
				statement.setString(5, element.getDigest());
				statement.setTimestamp(6, SQLSikevaDBTools.toTimestamp(element.getCreationDate()));
				statement.setTimestamp(7, SQLSikevaDBTools.toTimestamp(element.getEditionDate()));
				statement.setTimestamp(8, SQLSikevaDBTools.toTimestamp(element.getArchiveDate()));

				statement.executeUpdate();

				connection.commit();
			}
			catch (SQLException exception)
			{
				logger.error("Error putting element.", exception);
				try
				{
					connection.rollback();
				}
				catch (SQLException exception1)
				{
					logger.error("Rollback failed.", exception);
				}
				throw new SikevaDBException("Error putting element", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#put(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void put(final String key, final String subkey, final String value) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Key is null.");
		}
		else if (subkey == null)
		{
			throw new IllegalArgumentException("Subkey is null.");
		}
		else
		{
			//
			Element element = getElement(key, subkey);

			//
			if (element == null)
			{
				element = new Element();
				element.setKey(key);
				element.setSubkey(subkey);
				element.update(value);
				element.setArchiveDate(null);
			}
			else
			{
				element.update(value);
			}

			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(false);

				// Archive existing element.
				if (element.getId() != Element.NO_ID)
				{
					statement = connection.prepareStatement("UPDATE sikevadb_elements SET ARCHIVE_DATE=? WHERE TOPKEY=? AND SUBKEY=? AND ARCHIVE_DATE IS NULL");

					statement.setTimestamp(1, SQLSikevaDBTools.toTimestamp(element.getEditionDate()));
					statement.setString(2, element.getKey());
					statement.setString(3, element.getSubkey());

					statement.executeUpdate();
					statement.clearParameters();
					statement.close();
				}

				// Insert new version of the element.
				statement = connection.prepareStatement("INSERT INTO sikevadb_elements (TOPKEY,SUBKEY,VALUE,SIZE,DIGEST,CREATION_DATE,EDITION_DATE,ARCHIVE_DATE) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

				statement.setString(1, element.getKey());
				statement.setString(2, element.getSubkey());
				statement.setString(3, element.getValue());
				statement.setLong(4, element.getSize());
				statement.setString(5, element.getDigest());
				statement.setTimestamp(6, SQLSikevaDBTools.toTimestamp(element.getCreationDate()));
				statement.setTimestamp(7, SQLSikevaDBTools.toTimestamp(element.getEditionDate()));
				statement.setTimestamp(8, SQLSikevaDBTools.toTimestamp(element.getArchiveDate()));

				statement.executeUpdate();

				connection.commit();
			}
			catch (Exception exception)
			{
				logger.error("Error putting element.", exception);
				try
				{
					connection.rollback();
				}
				catch (SQLException exception1)
				{
					logger.error("Rollback failed.", exception);
				}
				throw new SikevaDBException("Error putting element", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#renameKey(java.lang.String, java.lang.String)
	 */
	@Override
	public void renameKey(final String oldKey, final String newKey) throws SikevaDBException
	{
		logger.info("renameKey starting... [{}][{}]", oldKey, newKey);

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (oldKey == null)
		{
			throw new IllegalArgumentException("OldKey is null.");
		}
		else if (newKey == null)
		{
			throw new IllegalArgumentException("NewKey is null.");
		}
		else
		{
			//
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				//
				connection = getConnection();
				connection.setAutoCommit(true);
				statement = connection.prepareStatement("UPDATE sikevadb_elements SET TOPKEY=? WHERE TOPKEY=?");

				statement.setString(1, newKey);
				statement.setString(2, oldKey);

				statement.executeUpdate();
			}
			catch (SQLException exception)
			{
				logger.error("Error renaming subkey.", exception);
				throw new SikevaDBException("Error renaming subkey", exception);
			}
			finally
			{
				closeQuietly(connection, statement, resultSet);
			}
		}

		logger.info("renameKey done.");
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#renameSubkey(java.lang.String, long, long)
	 */
	@Override
	public void renameSubkey(final String key, final long oldSubkey, final long newSubkey) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			renameSubkey(key, String.valueOf(oldSubkey), String.valueOf(newSubkey));
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#renameSubkey(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void renameSubkey(final String key, final String oldKey, final String newKey) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if ((key == null) || (oldKey == null) || (newKey == null))
		{
			throw new IllegalArgumentException("Null parameter detected [key=" + key + "][oldKey=" + oldKey + "][newKey=" + newKey + "].");
		}
		else
		{
			// TODO Auto-generated method stub
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#replaceInValue(java.lang.String, java.lang.String[])
	 */
	@Override
	public void replaceInValue(final String key, final String... tokens) throws SikevaDBException
	{
		logger.info("replaceInValue starting... [{}]", key);

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			//
			String value = getValue(key);

			//
			for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex += 2)
			{
				value = value.replaceAll(tokens[tokenIndex], tokens[tokenIndex + 1]);
			}

			//
			put(key, value);
		}

		logger.info("replaceInValue done.");
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#replaceInValues(java.lang.String, java.lang.String[])
	 */
	@Override
	public void replaceInValues(final String key, final String... tokens) throws SikevaDBException
	{
		logger.info("replaceInValues starting... [{}]", key);

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			Elements elements = getElements(key);

			long count = 0;
			for (Element element : elements)
			{
				logger.info(element.getKey() + " (" + element.getSubkey() + ") " + count + "/" + elements.size());

				if (element.getSubkey() != null)
				{
					//
					count += 1;

					//
					String value = element.getValue();

					//
					for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex += 2)
					{
						value = value.replaceAll(tokens[tokenIndex], tokens[tokenIndex + 1]);
					}

					//
					put(element.getKey(), element.getSubkey(), value);
				}
			}
		}

		logger.info("replaceInValues done.");
	}

	/**
	 * Sets the driver classname.
	 * 
	 * @param driverClassname
	 *            the new driver classname
	 */
	public void setDriverClassname(final String driverClassname)
	{
		this.driverClassname = driverClassname;
	}

	/**
	 * Sets the login.
	 * 
	 * @param login
	 *            the new login
	 */
	public void setLogin(final String login)
	{
		this.login = login;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

	/**
	 * Sets the url.
	 * 
	 * @param url
	 *            the new url
	 */
	public void setUrl(final String url)
	{
		this.url = url;
	}
}
