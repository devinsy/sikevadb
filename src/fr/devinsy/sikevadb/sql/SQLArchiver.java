/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.sql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.sikevadb.core.Archiver;
import fr.devinsy.sikevadb.core.Element;
import fr.devinsy.sikevadb.core.SikevaDBException;

/**
 * The Class SQLArchiver.
 * 
 * @author Christian Pierre MOMON
 */

public class SQLArchiver implements Archiver
{
	private static final Logger logger = LoggerFactory.getLogger(SQLArchiver.class);;

	private Status status;
	private SQLSikevaDB database;

	/**
	 * Instantiates a new SQL archiver.
	 * 
	 * @param source
	 *            the source
	 */
	public SQLArchiver(final SQLSikevaDB source)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.status = Status.ACTIVATED;
			this.database = source;
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#activate()
	 */
	@Override
	public void activate()
	{
		this.status = Status.ACTIVATED;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#archive(fr.devinsy.sikevadb.core.Element)
	 */
	@Override
	public void archive(final Element element) throws SikevaDBException
	{
		// TODO
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#clear()
	 */
	@Override
	public void clear() throws SikevaDBException
	{
		// TODO
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#close()
	 */
	@Override
	public void close()
	{
		// TODO
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#isActivated()
	 */
	@Override
	public boolean isActivated()
	{
		boolean result;

		if (this.status == Status.ACTIVATED)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#isSuspended()
	 */
	@Override
	public boolean isSuspended()
	{
		boolean result;

		result = !isActivated();

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#suspend()
	 */
	@Override
	public void suspend()
	{
		this.status = Status.SUSPENDED;
	}
}
