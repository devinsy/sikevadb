/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.filetree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.sikevadb.core.Element;
import fr.devinsy.sikevadb.core.SikevaDBException;
import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLWriter;

/**
 * The Class FileTreeSikevaDBTools.
 * 
 * @author Christian Pierre MOMON
 */

public class FileTreeSikevaDBTools
{
	private static final Logger LOGGER = LoggerFactory.getLogger(FileTreeSikevaDBTools.class);

	/**
	 * Load element.
	 * 
	 * @param source
	 *            the source
	 * @return the element
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public static Element loadElement(final File source) throws SikevaDBException
	{
		Element result;

		if (source == null)
		{
			result = null;
		}
		else if (!source.isFile())
		{
			result = null;
		}
		else
		{
			XMLReader in = null;
			try
			{
				result = new Element();

				in = new XMLReader(source);

				in.readXMLHeader();

				in.readStartTag("element");
				{
					// private long id; IN
					long id = Long.parseLong(in.readContentTag("id").getContent());
					result.setId(id);

					// private DateTime creationDate; IN
					DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());
					result.setCreationDate(creationDate);

					// private DateTime editionDate; IN
					DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());
					result.setEditionDate(editionDate);

					// private long size; IN
					long size = Long.parseLong(in.readContentTag("size").getContent());
					result.setSize(size);

					// private String digest; IN
					String digest = in.readContentTag("digest").getContent();
					result.setDigest(digest);

					// private String value; IN
					String value = in.readContentTag("value").getContent();
					result.setValue(value);
				}
				in.readEndTag("element");
			}
			catch (XMLStreamException exception)
			{
				throw new SikevaDBException("Error loading element.", exception);
			}
			catch (XMLBadFormatException exception)
			{
				throw new SikevaDBException("Error loading element.", exception);
			}
			catch (FileNotFoundException exception)
			{
				throw new SikevaDBException("Error loading element.", exception);
			}
			finally
			{
				if (in != null)
				{
					try
					{
						in.close();
					}
					catch (XMLStreamException exception)
					{
						LOGGER.error("Finally close failed.", exception);
					}

				}
			}
		}

		//
		return result;
	}

	/**
	 * Save element.
	 * 
	 * @param target
	 *            the target
	 * @param source
	 *            the source
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public static void saveElement(final File target, final Element source) throws SikevaDBException
	{
		LOGGER.debug("[target={}]", target);

		if (target == null)
		{
			throw new IllegalArgumentException("Target file is null.");
		}
		else if (target.isDirectory())
		{
			throw new IllegalArgumentException("Target is a directory.");
		}
		else
		{
			XMLWriter out = null;
			try
			{
				out = new XMLWriter(target);

				out.writeXMLHeader();

				out.writeStartTag("element");
				{
					// private long id; IN
					out.writeTag("id", source.getId());

					// private DateTime creationDate; IN
					out.writeTag("creation_date", source.getCreationDate().toString());

					// private DateTime editionDate; IN
					out.writeTag("edition_date", source.getEditionDate().toString());

					// private long size; IN
					out.writeTag("size", source.getSize());

					// private String digest; IN
					out.writeTag("digest", source.getDigest());

					// private String value; IN
					out.writeTag("value", source.getValue());
				}
				out.writeEndTag("element");
			}
			catch (UnsupportedEncodingException exception)
			{
				throw new SikevaDBException("Error saving element.", exception);
			}
			catch (FileNotFoundException exception)
			{
				throw new SikevaDBException("File Not Found saving element.", exception);
			}
			finally
			{
				if (out != null)
				{
					try
					{
						out.close();
					}
					catch (IOException exception)
					{
						LOGGER.error("Finally close failed.", exception);
					}
				}
			}
		}
	}
}
