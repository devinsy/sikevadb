/*
 * Copyright (C) 2013-2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.filetree;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.sikevadb.core.Archiver;
import fr.devinsy.sikevadb.core.ClosedDatabaseException;
import fr.devinsy.sikevadb.core.Element;
import fr.devinsy.sikevadb.core.Elements;
import fr.devinsy.sikevadb.core.OpenedDatabaseException;
import fr.devinsy.sikevadb.core.SikevaDB;
import fr.devinsy.sikevadb.core.SikevaDBException;
import fr.devinsy.util.strings.StringList;
import fr.devinsy.util.strings.StringsUtils;

/**
 * The Class FileTreeSikevaDB.
 * 
 * <pre>
 * private long id;					IN
 * private DateTime creationDate;	IN
 * private DateTime editionDate;	IN
 * private DateTime archiveDate;	OUT		DEPRECATED
 * private String key;				IN/OUT
 * private String subkey;			IN/OUT
 * private long size;				IN
 * private String digest;			IN
 * private String value;			IN
 * </pre>
 * 
 * @author Christian Pierre MOMON
 */
public class FileTreeSikevaDB implements SikevaDB
{
	public enum Status
	{
		OPENED,
		CLOSED
	};

	private static final Logger logger = LoggerFactory.getLogger(FileTreeSikevaDB.class);

	private static final DateTimeFormatter ISOFormatter = ISODateTimeFormat.dateHourMinuteSecondMillis();

	private Status status;
	private String login;
	private String password;
	private File homeDirectory;
	private File dataDirectory;
	private Archiver archiver;
	private File configDirectory;

	/**
	 * Instantiates a new file tree SikevaDB.
	 * 
	 * @param homeDirectory
	 *            the home directory
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public FileTreeSikevaDB(final File homeDirectory, final String login, final String password) throws SikevaDBException
	{
		if (homeDirectory == null)
		{
			throw new SikevaDBException("Invalid home directory (null).");
		}
		else if (StringUtils.equals(homeDirectory.getAbsolutePath(), "/"))
		{
			throw new SikevaDBException("Invalid home directory (file system root).");
		}
		else
		{
			this.status = Status.CLOSED;
			this.homeDirectory = homeDirectory;
			this.dataDirectory = new File(this.homeDirectory, "data");
			this.configDirectory = new File(this.homeDirectory, "config");
			this.login = login;
			this.password = password;
			this.archiver = null;
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#archiver()
	 */
	@Override
	public Archiver archiver() throws SikevaDBException
	{
		Archiver result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = this.archiver;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#clear()
	 */
	@Override
	public void clear() throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			try
			{
				// TODO journalize("clear database");

				FileUtils.deleteDirectory(this.dataDirectory);
				this.dataDirectory.mkdir();
			}
			catch (IOException exception)
			{
				this.logger.error("Error clearing database", exception);
				throw new SikevaDBException("Error clearing database", exception);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#close()
	 */
	@Override
	public void close() throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			this.archiver.close();

			this.status = Status.CLOSED;
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#countOfElements()
	 */
	@Override
	public long countOfElements() throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			File[] topFiles = this.dataDirectory.listFiles();

			result = 0;
			for (File file : topFiles)
			{
				if (file.isDirectory())
				{
					File[] subFiles = file.listFiles();
					result += subFiles.length;
				}
				else
				{
					result += 1;
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#countOfElements(java.lang.String)
	 */
	@Override
	public long countOfElements(final String key) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			File targetDirectory = new File(this.dataDirectory, key);
			File[] subFiles = targetDirectory.listFiles();

			if (subFiles == null)
			{
				result = 1;
			}
			else
			{
				result = subFiles.length;
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#countOfElements(java.lang.String, long)
	 */
	@Override
	public long countOfElements(final String key, final long subkey) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = countOfElements(key, String.valueOf(subkey));
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#countOfElements(java.lang.String, java.lang.String)
	 */
	@Override
	public long countOfElements(final String key, final String subkey) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if ((key == null) || (subkey == null))
		{
			throw new IllegalArgumentException("Null key detected [key=" + key + "][subkey=" + subkey + "].");
		}
		else
		{
			File targetDirectory = new File(this.dataDirectory, key);
			File targetFile = new File(targetDirectory, subkey);
			if (targetFile.exists())
			{
				result = 1;
			}
			else
			{
				result = 0;
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#create()
	 */
	@Override
	public void create() throws SikevaDBException
	{
		if (isOpened())
		{
			throw new OpenedDatabaseException();
		}
		else if (this.homeDirectory == null)
		{
			throw new SikevaDBException("Invalid root directory.");
		}
		else if (this.homeDirectory.exists())
		{
			throw new SikevaDBException("Root directory already is existing.");
		}
		else
		{
			this.homeDirectory.mkdir();
			this.dataDirectory.mkdir();
			this.configDirectory.mkdir();
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#delete(java.lang.String)
	 */
	@Override
	public void delete(final String key) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			File targetFile = new File(this.dataDirectory, key);

			if (targetFile.isFile())
			{
				boolean result = targetFile.delete();
				if (!result)
				{
					throw new SikevaDBException("Error deleting [" + targetFile.getAbsolutePath() + "]");
				}
			}
			else
			{
				throw new SikevaDBException("Invalid target to delete [" + targetFile.getAbsolutePath() + "]");
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#delete(java.lang.String, long)
	 */
	@Override
	public void delete(final String key, final long subkey) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			delete(key, String.valueOf(subkey));
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#delete(java.lang.String, java.lang.String)
	 */
	@Override
	public void delete(final String key, final String subkey) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if ((key == null) || (subkey == null))
		{
			throw new IllegalArgumentException("Null key detected [key=" + key + "][subkey=" + subkey + "].");
		}
		else
		{
			File targetDirectory = new File(this.dataDirectory, key);
			File targetFile = new File(targetDirectory, subkey);

			if (targetFile.isFile())
			{
				boolean result = targetFile.delete();
				if (!result)
				{
					throw new SikevaDBException("Error deleting [" + targetFile.getAbsolutePath() + "]");
				}
			}
			else
			{
				throw new SikevaDBException("Invalid target to deleting [" + targetFile.getAbsolutePath() + "]");
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#deleteMany(java.lang.String, java.lang.String[])
	 */
	@Override
	public void deleteMany(final String key, final String... subkeys) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			if ((subkeys != null) && (subkeys.length > 0))
			{
				for (String subkey : subkeys)
				{
					delete(key, subkey);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#destroy()
	 */
	@Override
	public void destroy() throws SikevaDBException
	{
		if (isOpened())
		{
			throw new OpenedDatabaseException();
		}
		else if (this.homeDirectory == null)
		{
			throw new SikevaDBException("Invalid home directory (undefined), destroy operation is cancelled.");
		}
		else
		{
			String databaseHome = this.homeDirectory.getAbsolutePath();
			databaseHome = StringUtils.removeEnd(databaseHome, "/.");

			if (StringUtils.isBlank(databaseHome))
			{
				throw new SikevaDBException("Invalid home directory (blank), destroy operation is cancelled.");
			}
			else if (this.dataDirectory == null)
			{
				throw new SikevaDBException("Invalid data directory (undefined), destroy operation is cancelled.");
			}
			else if (StringUtils.isBlank(this.dataDirectory.getAbsolutePath()))
			{
				throw new SikevaDBException("Invalid data directory (blank), destroy operation is cancelled.");
			}
			else if (this.configDirectory == null)
			{
				throw new SikevaDBException("Invalid config directory (undefined), destroy operation is cancelled.");
			}
			else if (StringUtils.isBlank(this.configDirectory.getAbsolutePath()))
			{
				throw new SikevaDBException("Invalid config directory (blank), destroy operation is cancelled.");
			}
			else if (StringsUtils.equalsAny(databaseHome, "/", "/root", "/usr", "/home", "/tmp", "/var", "/srv", "/boot"))
			{
				throw new SikevaDBException("Invalid home directory (system file), destroy operation is cancelled.");
			}
			else if (StringUtils.equals(databaseHome, SystemUtils.USER_HOME))
			{
				throw new SikevaDBException("Invalid config directory (), destroy operation is cancelled.");
			}
			else if (exists())
			{
				try
				{
					FileUtils.deleteDirectory(this.homeDirectory);
				}
				catch (IOException exception)
				{
					throw new SikevaDBException("Error destroying database. You have to achieve the operation manually.", exception);
				}
			}
			else
			{
				throw new SikevaDBException("Impossible to destroy a none existing database.");
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isCreated()
	 */
	@Override
	public boolean exists() throws SikevaDBException
	{
		boolean result;

		if ((this.homeDirectory.isDirectory()) && (this.dataDirectory.isDirectory()) && (this.configDirectory.isDirectory()))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Gets the config directory.
	 * 
	 * @return the config directory
	 */
	public File getConfigDirectory()
	{
		File result;

		result = this.configDirectory;

		//
		return result;
	}

	/**
	 * Gets the data directory.
	 * 
	 * @return the data directory
	 */
	public File getDataDirectory()
	{
		File result;

		result = this.dataDirectory;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElement(java.lang.String)
	 */
	@Override
	public Element getElement(final String key) throws SikevaDBException
	{
		Element result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			File targetFile = new File(this.dataDirectory, key);
			result = FileTreeSikevaDBTools.loadElement(targetFile);
			if (result != null)
			{
				result.setKey(key);
				result.setSubkey(null);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElement(java.lang.String, long)
	 */
	@Override
	public Element getElement(final String key, final long subkey) throws SikevaDBException
	{
		Element result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = getElement(key, String.valueOf(subkey));
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElement(java.lang.String, java.lang.String)
	 */
	@Override
	public Element getElement(final String key, final String subkey) throws SikevaDBException
	{
		Element result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if ((key == null) || (subkey == null))
		{
			throw new IllegalArgumentException("Null key detected [key=" + key + "][subkey=" + subkey + "].");
		}
		else
		{
			//
			File targetDirectory = new File(this.dataDirectory, key);
			File targetFile = new File(targetDirectory, subkey);

			result = FileTreeSikevaDBTools.loadElement(targetFile);
			if (result != null)
			{
				result.setKey(key);
				result.setSubkey(subkey);
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElements()
	 */
	@Override
	public Elements getElements() throws SikevaDBException
	{
		Elements result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = new Elements((int) countOfElements());

			File[] topFiles = this.dataDirectory.listFiles();
			for (File file : topFiles)
			{
				if (file.isFile())
				{
					Element element = getElement(file.getName());

					result.add(element);
				}
				else if (file.isDirectory())
				{
					File[] subFiles = file.listFiles();

					for (File subFile : subFiles)
					{
						if (subFile.isFile())
						{
							Element element = getElement(file.getName(), subFile.getName());

							result.add(element);
						}
					}
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getElements(java.lang.String)
	 */
	@Override
	public Elements getElements(final String key) throws SikevaDBException
	{
		Elements result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			result = new Elements((int) countOfElements(key));

			File targetDirectory = new File(this.dataDirectory, key);
			if (targetDirectory.exists())
			{
				File[] files = targetDirectory.listFiles();
				for (File subfile : files)
				{
					Element element = getElement(key, subfile.getName());

					result.add(element);
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the file tree directory.
	 * 
	 * @return the file tree directory
	 */
	public File getFileTreeDirectory()
	{
		File result;

		result = this.homeDirectory;

		//
		return result;
	}

	/**
	 * Gets the login.
	 * 
	 * @return the login
	 */
	public String getLogin()
	{
		String result;

		result = this.login;

		//
		return result;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword()
	{
		String result;

		result = this.password;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getSubkeys(java.lang.String)
	 */
	@Override
	public StringList getSubkeys(final String key) throws SikevaDBException
	{
		StringList result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			//
			result = new StringList();

			File targetDirectory = new File(this.dataDirectory, key);
			if ((targetDirectory.exists()) && (targetDirectory.isDirectory()))
			{
				File[] subfiles = targetDirectory.listFiles();

				for (File subfile : subfiles)
				{
					result.add(subfile.getName());
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getKeys()
	 */
	@Override
	public StringList getTopKeys() throws SikevaDBException
	{
		StringList result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = new StringList((int) countOfElements());

			File[] topFiles = this.dataDirectory.listFiles();
			for (File file : topFiles)
			{
				if (file.isFile())
				{
					result.add(file.getName());
				}
				else if (file.isDirectory())
				{
					result.add(file.getName());
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getValue(java.lang.String)
	 */
	@Override
	public String getValue(final String key) throws SikevaDBException
	{
		String result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			File targetFile = new File(this.dataDirectory, key);
			if (targetFile.exists())
			{
				Element element = FileTreeSikevaDBTools.loadElement(targetFile);

				result = element.getValue();
			}
			else
			{
				result = null;
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getValue(java.lang.String, long)
	 */
	@Override
	public String getValue(final String key, final long subkey) throws SikevaDBException
	{
		String result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = getValue(key, String.valueOf(subkey));
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getValue(java.lang.String, java.lang.String)
	 */
	@Override
	public String getValue(final String key, final String subkey) throws SikevaDBException
	{
		String result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if ((key == null) || (subkey == null))
		{
			throw new IllegalArgumentException("Null key detected [key=" + key + "][subkey=" + subkey + "].");
		}
		else
		{
			File targetDirectory = new File(this.dataDirectory, key);
			File targetFile = new File(targetDirectory, subkey);
			if (targetFile.exists())
			{
				Element element = FileTreeSikevaDBTools.loadElement(targetFile);

				result = element.getValue();
			}
			else
			{
				result = null;
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#getValues(java.lang.String)
	 */
	@Override
	public StringList getValues(final String key) throws SikevaDBException
	{
		StringList result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			//
			result = new StringList((int) countOfElements(key));

			//
			File targetDirectory = new File(this.dataDirectory, key);

			if (targetDirectory.isDirectory())
			{
				File[] subFiles = targetDirectory.listFiles();

				for (File subFile : subFiles)
				{
					if (subFile.isFile())
					{
						Element element = FileTreeSikevaDBTools.loadElement(subFile);

						result.add(element.getValue());
					}
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isArchiveActivated()
	 */
	@Override
	public boolean isArchiveActivated()
	{
		boolean result;

		if (isOpened())
		{
			result = this.archiver.isActivated();
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isArchiveSuspended()
	 */
	@Override
	public boolean isArchiveSuspended()
	{
		boolean result;

		result = !isArchiveActivated();

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isClosed()
	 */
	@Override
	public boolean isClosed()
	{
		boolean result;

		result = !isOpened();

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#isOpened()
	 */
	@Override
	public boolean isOpened()
	{
		boolean result;

		if (this.status == Status.OPENED)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#memorySize()
	 */
	@Override
	public long memorySize() throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			//
			result = 0;

			//
			File[] topFiles = this.dataDirectory.listFiles();
			for (File file : topFiles)
			{
				if (file.isFile())
				{
					Element element = FileTreeSikevaDBTools.loadElement(file);

					result += element.getSize();
				}
				else if (file.isDirectory())
				{
					File[] subFiles = file.listFiles();

					for (File subFile : subFiles)
					{
						if (subFile.isFile())
						{
							Element element = FileTreeSikevaDBTools.loadElement(subFile);

							result += element.getSize();
						}
					}
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#memorySize(java.lang.String)
	 */
	@Override
	public long memorySize(final String key) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			if (key == null)
			{
				throw new IllegalArgumentException("Null key detected.");
			}
			else
			{
				File targetFile = new File(this.dataDirectory, key);
				if (targetFile.isFile())
				{
					Element element = getElement(key);

					result = element.getSize();
				}
				else if (targetFile.isDirectory())
				{
					result = 0;
					for (File subFile : targetFile.listFiles())
					{
						Element element = getElement(key, subFile.getName());

						result += element.getSize();
					}
				}
				else
				{
					result = 0;
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#memorySize(java.lang.String, long)
	 */
	@Override
	public long memorySize(final String key, final long subkey) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			result = memorySize(key, String.valueOf(subkey));
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#memorySize(java.lang.String, java.lang.String)
	 */
	@Override
	public long memorySize(final String key, final String subkey) throws SikevaDBException
	{
		long result;

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			if ((key == null) || (subkey == null))
			{
				throw new IllegalArgumentException("Null key detected [key=" + key + "][subkey=" + subkey + "].");
			}
			else
			{
				File targetDirectory = new File(this.dataDirectory, key);
				File targetFile = new File(targetDirectory, subkey);

				if (targetFile.isFile())
				{
					Element element = FileTreeSikevaDBTools.loadElement(targetFile);

					result = element.getSize();
				}
				else
				{
					result = 0;
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#open()
	 */
	@Override
	public void open() throws SikevaDBException
	{
		if (exists())
		{
			if (isOpened())
			{
				throw new SikevaDBException("Database already opened.");
			}
			else
			{
				this.status = Status.OPENED;
				this.archiver = new FileTreeArchiver(this);
			}
		}
		else
		{
			throw new SikevaDBException("Database is not existing.");
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#put(fr.devinsy.sikevadb.core.Element)
	 */
	@Override
	public void put(final Element element) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (element == null)
		{
			throw new IllegalArgumentException("element is null.");
		}
		else if (element.getKey() == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			if (element.getSubkey() == null)
			{
				File targetFile = new File(this.dataDirectory, element.getKey());
				FileTreeSikevaDBTools.saveElement(targetFile, element);
			}
			else
			{
				File targetDirectory = new File(this.dataDirectory, element.getKey());
				if (!targetDirectory.exists())
				{
					boolean result = targetDirectory.mkdir();
					if (result == false)
					{
						throw new SikevaDBException("Error creating key directory [" + targetDirectory + "]");
					}
				}
				else if (!targetDirectory.isDirectory())
				{
					throw new SikevaDBException("Invalid key directory [" + element.getKey() + "]");
				}

				File targetFile = new File(targetDirectory, element.getSubkey());
				FileTreeSikevaDBTools.saveElement(targetFile, element);
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#put(java.lang.String, long, java.lang.String)
	 */
	@Override
	public void put(final String key, final long subkey, final String value) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			put(key, String.valueOf(subkey), value);
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#put(java.lang.String, java.lang.String)
	 */
	@Override
	public void put(final String key, final String value) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Null key detected.");
		}
		else
		{
			//
			Element element = getElement(key);

			//
			if (element == null)
			{
				element = new Element();
				element.setKey(key);
				element.update(value);
				element.setArchiveDate(null);
			}
			else
			{
				element.update(value);
			}

			//
			put(element);
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#put(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void put(final String key, final String subkey, final String value) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if ((key == null) || (subkey == null))
		{
			throw new IllegalArgumentException("Null key detected [key=" + key + "][subkey=" + subkey + "].");
		}
		else
		{
			//
			Element element = getElement(key, subkey);

			//
			if (element == null)
			{
				element = new Element();
				element.setKey(key);
				element.setSubkey(subkey);
				element.update(value);
				element.setArchiveDate(null);
			}
			else
			{
				element.update(value);
			}

			//
			put(element);
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#renameKey(java.lang.String, java.lang.String)
	 */
	@Override
	public void renameKey(final String oldKey, final String newKey) throws SikevaDBException
	{
		this.logger.debug("renameKey starting... [{}][{}]", oldKey, newKey);

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (oldKey == null)
		{
			throw new IllegalArgumentException("OldKey is null.");
		}
		else if (newKey == null)
		{
			throw new IllegalArgumentException("OldKey is null.");
		}
		else
		{
			File oldFile = new File(this.dataDirectory, oldKey);
			File newFile = new File(this.dataDirectory, newKey);

			if (oldFile.isFile())
			{
				if (newFile.isFile())
				{
					throw new IllegalArgumentException("Invalid newKey [" + newKey + "].");
				}
				else
				{
					oldFile.renameTo(newFile);
				}
			}
			else
			{
				oldFile.renameTo(newFile);
			}
		}

		this.logger.debug("renameKey done.");
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#renameSubkey(java.lang.String, long, long)
	 */
	@Override
	public void renameSubkey(final String key, final long oldSubKey, final long newSubkey) throws SikevaDBException
	{
		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			renameSubkey(key, String.valueOf(oldSubKey), String.valueOf(newSubkey));
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#renameSubkey(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void renameSubkey(final String key, final String oldSubkey, final String newSubkey) throws SikevaDBException
	{
		this.logger.debug("renameSybKey starting... [{}][{}][{}]", oldSubkey, newSubkey);

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else if (key == null)
		{
			throw new IllegalArgumentException("Top key is null.");
		}
		else if (oldSubkey == null)
		{
			throw new IllegalArgumentException("OldKey is null.");
		}
		else if (newSubkey == null)
		{
			throw new IllegalArgumentException("OldKey is null.");
		}
		else
		{
			File targetDirectory = new File(this.dataDirectory, key);
			File oldFile = new File(targetDirectory, oldSubkey);
			File newFile = new File(targetDirectory, newSubkey);

			if (oldFile.isFile())
			{
				if (newFile.isFile())
				{
					throw new IllegalArgumentException("Invalid newKey [" + newSubkey + "].");
				}
				else
				{
					oldFile.renameTo(newFile);
				}
			}
			else
			{
				throw new IllegalArgumentException("Invalid oldKey [" + oldSubkey + "].");
			}
		}

		this.logger.debug("renameSubKey done.");
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#replaceInValue(java.lang.String, java.lang.String[])
	 */
	@Override
	public void replaceInValue(final String key, final String... tokens) throws SikevaDBException
	{
		this.logger.info("replaceInValue starting... [{}]", key);

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			String value = getValue(key);

			for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex += 2)
			{
				value = value.replaceAll(tokens[tokenIndex], tokens[tokenIndex + 1]);
			}

			put(key, value);
		}

		this.logger.info("replaceInValue done.");
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.SikevaDB#replaceInValues(java.lang.String, java.lang.String[])
	 */
	@Override
	public void replaceInValues(final String key, final String... tokens) throws SikevaDBException
	{
		this.logger.info("replaceInValues starting... [{}]", key);

		if (isClosed())
		{
			throw new ClosedDatabaseException();
		}
		else
		{
			Elements elements = getElements(key);

			long count = 0;
			for (Element element : elements)
			{
				this.logger.info(element.getKey() + " (" + element.getSubkey() + ") " + count + "/" + elements.size());

				if (element.getSubkey() != null)
				{
					//
					count += 1;

					//
					String value = element.getValue();

					//
					for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex += 2)
					{
						value = value.replaceAll(tokens[tokenIndex], tokens[tokenIndex + 1]);
					}

					//
					put(element.getKey(), element.getSubkey(), value);
				}
			}
		}

		this.logger.info("replaceInValues done.");
	}

	/**
	 * Sets the login.
	 * 
	 * @param login
	 *            the new login
	 */
	public void setLogin(final String login)
	{
		this.login = login;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}
}
