/*
 * Copyright (C) 2013-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.filetree;

import java.io.File;

import fr.devinsy.sikevadb.core.Archiver;
import fr.devinsy.sikevadb.core.Element;
import fr.devinsy.sikevadb.core.SikevaDB;
import fr.devinsy.sikevadb.core.SikevaDBException;

/**
 * The Class FileTreeArchiver.
 * 
 * @author Christian Pierre MOMON
 */

public class FileTreeArchiver implements Archiver
{
	private static final String ARCHIVE_PATH = "archive";

	private Status status;
	private File archiveDirectory;
	private SikevaDB database;

	/**
	 * Instantiates a new file tree archiver.
	 * 
	 * @param source
	 *            the source
	 * 
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	public FileTreeArchiver(final FileTreeSikevaDB source) throws SikevaDBException
	{
		if (source == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.status = Status.ACTIVATED;
			this.archiveDirectory = new File(source.getFileTreeDirectory(), ARCHIVE_PATH);
			if (this.archiveDirectory.exists())
			{
				if (!this.archiveDirectory.isDirectory())
				{
					throw new SikevaDBException("Archive directory unavailable.");
				}
			}
			else
			{
				this.archiveDirectory.mkdir();
			}
			this.database = source;
		}
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#activate()
	 */
	@Override
	public void activate()
	{
		this.status = Status.ACTIVATED;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#archive(fr.devinsy.sikevadb.core.Element)
	 */
	@Override
	public void archive(final Element element) throws SikevaDBException
	{
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#clear()
	 */
	@Override
	public void clear() throws SikevaDBException
	{
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#close()
	 */
	@Override
	public void close()
	{
		// TODO
	}

	/**
	 * Gets the path.
	 * 
	 * @return the path
	 */
	public File getPath()
	{
		return this.archiveDirectory;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#isActivated()
	 */
	@Override
	public boolean isActivated()
	{
		boolean result;

		if (this.status == Status.ACTIVATED)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#isSuspended()
	 */
	@Override
	public boolean isSuspended()
	{
		boolean result;

		result = !isActivated();

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.devinsy.sikevadb.core.Archiver#suspend()
	 */
	@Override
	public void suspend()
	{
		this.status = Status.SUSPENDED;
	}
}
