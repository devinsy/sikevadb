/*
 * Copyright (C) 2013-2018 Christian Pierre MOMON, DEVINSY
 * 
 * This file is part of SikevaDB, simple key value database.
 * 
 * SikevaDB is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SikevaDB is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with SikevaDB. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.sikevadb.filetree;

import java.io.File;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.sikevadb.core.Element;
import fr.devinsy.sikevadb.core.Elements;
import fr.devinsy.sikevadb.core.SikevaDBException;
import fr.devinsy.sikevadb.core.XMLSikevaDB;
import fr.devinsy.util.strings.StringList;

/**
 * The Class TreeFileSikevaDBTest.
 * 
 * @author Christian Pierre MOMON
 */
public class TreeFileSikevaDBTest
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TreeFileSikevaDBTest.class);
	private static FileTreeSikevaDB database;

	@Test
	public void testExport01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		database.put("alpha01", "qlskjfmlqja");
		database.put("alpha01", "qlskjfmlqjb");
		database.put("alpha02", "qlskjfmlqj");
		database.put("alpha03", "qlskjfmlqj");
		database.put("alpha04", "qlskjfmlqj");
		database.put("alpha05", "qlskjfmlqj");
		database.delete("alpha03");
		database.put("alpha01s", "bravo1", "qlskjfmlqja");
		database.put("alpha01s", "bravo1", "qlskjfmlqjb");
		database.put("alpha01s", "bravo2", "qlskjfmlqj");
		database.put("alpha01s", "bravo3", "qlskjfmlqj");
		database.put("alpha01s", "bravo4", "qlskjfmlqj");
		database.put("alpha01s", "bravo5", "qlskjfmlqj");
		database.delete("alpha01s", "bravo3");

		File file = File.createTempFile("sikevadb-", ".tmp");
		XMLSikevaDB.export(file, database);
		Assert.assertTrue(file.exists());
		file.delete();

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test get keyse 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testGetKeyse01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		database.put("alpha01", "1234567890");
		database.put("alpha01", "qlskjfmlqj");
		database.put("alpha02", "qlskjfmlqj");
		database.put("alpha03", "qlskjfmlqj");
		database.put("alpha04", "qlskjfmlqj");
		database.put("alpha05", "qlskjfmlqj");
		database.put("alpha01s", "bravo1", "1234567890");
		database.put("alpha01s", "bravo1", "qlskjfmlqj");
		database.put("alpha01s", "bravo2", "qlskjfmlqj");
		database.put("alpha01s", "bravo3", "qlskjfmlqj");
		database.put("alpha01s", "bravo4", "qlskjfmlqj");
		database.put("alpha01s", "bravo5", "qlskjfmlqj");

		Assert.assertEquals(0, database.getSubkeys("none").size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test get keyse 02.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testGetKeyse02() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		database.put("alpha01", "1234567890");
		database.put("alpha01", "qlskjfmlqj");
		database.put("alpha02", "qlskjfmlqj");
		database.put("alpha03", "qlskjfmlqj");
		database.put("alpha04", "qlskjfmlqj");
		database.put("alpha05", "qlskjfmlqj");
		database.put("alpha01s", "bravo1", "1234567890");
		database.put("alpha01s", "bravo1", "qlskjfmlqj");
		database.put("alpha01s", "bravo2", "qlskjfmlqj");
		database.put("alpha01s", "bravo3", "qlskjfmlqj");
		database.put("alpha01s", "bravo4", "qlskjfmlqj");
		database.put("alpha01s", "bravo5", "qlskjfmlqj");

		StringList keys = database.getTopKeys();

		Assert.assertEquals(6, keys.size());
		Assert.assertTrue(keys.contains("alpha03"));
		Assert.assertEquals(0, database.getSubkeys("alpha03s").size());
		Assert.assertEquals(5, database.getSubkeys("alpha01s").size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test memory size 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testMemorySize01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		Assert.assertEquals(0, database.memorySize());

		database.put("alpha01", "1234567890");
		database.put("alpha01", "qlskjfmlqj");
		database.put("alpha02", "qlskjfmlqj");
		database.put("alpha03", "qlskjfmlqj");
		database.put("alpha04", "qlskjfmlqj");
		database.put("alpha05", "qlskjfmlqj");
		database.put("alpha01s", "bravo1", "1234567890");
		database.put("alpha01s", "bravo1", "qlskjfmlqj");
		database.put("alpha01s", "bravo2", "qlskjfmlqj");
		database.put("alpha01s", "bravo3", "qlskjfmlqj");
		database.put("alpha01s", "bravo4", "qlskjfmlqj");
		database.put("alpha01s", "bravo5", "qlskjfmlqj");

		Assert.assertEquals(100, database.memorySize());
		Assert.assertEquals(10, database.memorySize("alpha03"));
		Assert.assertEquals(50, database.memorySize("alpha01s"));
		Assert.assertEquals(10, database.memorySize("alpha01s", "bravo1"));

		database.clear();

		Assert.assertEquals(0, database.memorySize());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testPut01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		String source = "bonjour";
		database.put("alpha01", source);
		String target = database.getValue("alpha01");
		Assert.assertEquals(source, target);

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put 02.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testPut02() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		String source = "bonjour";
		database.put("alpha01", source);

		String target = database.getValue("alpha01");
		Assert.assertEquals(source, target);

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put 03.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testPut03() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		database.getValue("foo", null);

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put 04.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testPut04() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		String source = "bonjour";
		database.put("alpha02", "bravo", source);

		String target = database.getValue("alpha02", "bravo");
		Assert.assertEquals(source, target);

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put 05.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testPut05() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		String source = "bonjour";
		database.put("alpha01", source);

		String target = database.getValue("alpha01");
		Assert.assertEquals(source, target);

		String source2 = "au revoir";
		database.put("alpha01", source2);

		target = database.getValue("alpha01");
		Assert.assertEquals(source2, target);

		// TODO StringList targets = database.getArchivedValues("alpha01");
		// TODO Assert.assertEquals(1, targets.size());
		// TODO Assert.assertEquals(source, targets.get(0));

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put 06.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testPut06() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		String source = "bonjour";
		database.put("alpha01", "bravo", source);

		String target = database.getValue("alpha01", "bravo");
		Assert.assertEquals(source, target);

		String source2 = "au revoir";
		database.put("alpha01", "bravo", source2);

		target = database.getValue("alpha01", "bravo");
		Assert.assertEquals(source2, target);

		// TODO StringList targets = database.getArchivedValues("alpha01",
		// "bravo");
		// TODO Assert.assertEquals(1, targets.size());
		// TODO Assert.assertEquals(source, targets.get(0));

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put element 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testPutElement01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		Element element = new Element();

		element.setKey("alpha");
		element.setSubkey(null);
		element.setSize(10);
		element.setDigest("qsdkfqskjf");
		element.setCreationDate(DateTime.now().minusDays(11));
		element.setEditionDate(DateTime.now().minusDays(11));
		element.setArchiveDate(DateTime.now().minusDays(10));
		element.setValue("bonjour");

		database.put(element);

		Assert.assertEquals(1, database.countOfElements());
		// TODO Assert.assertEquals(1, database.countOfArchivedElements());
		// TODO Assert.assertEquals(1, database.countOfAllElements());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put element 02.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testPutElement02() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();
		logger.debug("count={}", database.countOfElements());

		Element element = new Element();

		element.setId(123);
		element.setKey("alpha");
		element.setSubkey(null);
		element.setSize(10);
		element.setDigest("qsdkfqskjf");
		element.setCreationDate(DateTime.now().minusDays(11));
		element.setEditionDate(DateTime.now().minusDays(11));
		element.setArchiveDate(DateTime.now().minusDays(10));
		element.setValue("bonjour");

		database.put(element);

		Assert.assertEquals(1, database.countOfElements());
		// TODO Assert.assertEquals(1, database.countOfArchivedElements());
		// TODO Assert.assertEquals(1, database.countOfAllElements());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test put element 03.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testPutElement03() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		{
			Element element = new Element();

			element.setId(123);
			element.setKey("alpha");
			element.setSubkey(null);
			element.setSize(10);
			element.setDigest("qsdkfqskjf");
			element.setCreationDate(DateTime.now().minusDays(11));
			element.setEditionDate(DateTime.now().minusDays(11));
			element.setArchiveDate(DateTime.now().minusDays(10));
			element.setValue("bonjour");

			database.put(element);
		}

		{
			Element element = new Element();

			element.setKey("bravo");
			element.setSubkey(null);
			element.setSize(10);
			element.setDigest("qsdkfqskjf");
			element.setCreationDate(DateTime.now().minusDays(11));
			element.setEditionDate(DateTime.now().minusDays(11));
			element.setArchiveDate(DateTime.now().minusDays(10));
			element.setValue("bonjour");

			database.put(element);
		}

		// TODO Elements elements = database.getAllElements();
		Elements elements = database.getElements();

		Assert.assertEquals(2, elements.size());
		Assert.assertEquals(2, database.countOfElements());
		// TODO Assert.assertEquals(2, database.countOfArchivedElements());
		// TODO Assert.assertEquals(2, database.countOfAllElements());

		// TODO Assert.assertEquals(123, elements.get(0).getId());
		// TODO Assert.assertEquals(124, elements.get(1).getId());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test random 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRandom01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		String surSource = RandomStringUtils.random(128);
		String source = org.apache.commons.codec.binary.Base64.encodeBase64String(surSource.getBytes());
		database.put("alpha01", source);

		String target = database.getValue("alpha01");
		Assert.assertEquals(source, target);

		String surTarget = new String(Base64.decodeBase64(target));
		Assert.assertEquals(surSource, surTarget);

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test remove 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRemove01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		String source = "bonjour";
		database.put("alpha01", "bravo", source);

		String target = database.getValue("alpha01", "bravo");
		Assert.assertEquals(source, target);

		String source2 = "au revoir";
		database.put("alpha01", "bravo", source2);

		target = database.getValue("alpha01", "bravo");
		Assert.assertEquals(source2, target);

		// TODO StringList targets = database.getArchivedValues("alpha01",
		// "bravo");
		// TODO Assert.assertEquals(1, targets.size());
		// TODO Assert.assertEquals(source, targets.get(0));

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test remove many 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRemoveMany01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		database.put("alpha01", "alpha", "Allo");
		database.put("alpha01", "bravo", "Bonjour");
		database.put("alpha01", "charlie", "Courage");
		database.put("alpha01", "delta", "Droiture");
		database.put("alpha01", "echo", "Europe");
		database.put("alpha01", "fox", "Force");

		database.put("alpha02", "alpha", "Allo");
		database.put("alpha02", "bravo", "Bonjour");
		database.put("alpha02", "charlie", "Courage");
		database.put("alpha02", "delta", "Droiture");
		database.put("alpha02", "echo", "Europe");
		database.put("alpha02", "fox", "Force");

		Assert.assertEquals(12, database.countOfElements());

		database.deleteMany("alpha01", "bravo", "delta", "fox");

		Assert.assertEquals(9, database.countOfElements());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test rename key 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRenameKey01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		database.put("alpha01", "val-alpha01");
		database.put("alpha01s", "sub-alpha01", "val-alpha01");
		database.put("alpha01s", "sub-alpha02", "val-alpha02");
		database.put("alpha01s", "sub-alpha03", "val-alpha03");
		database.put("alpha02s", "sub-alpha01", "val-alpha01");
		database.put("alpha03s", "sub-alpha02", "val-alpha02");
		database.put("alpha04s", "sub-alpha03", "val-alpha03");

		database.renameKey("alpha01s", "november");

		Elements targets = database.getElements();

		Assert.assertEquals(7, targets.size());
		Assert.assertEquals(3, database.getValues("november").size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test size 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testSize01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		Assert.assertEquals(0, database.countOfElements());
		// TODO Assert.assertEquals(0, database.countOfArchivedElements());
		// TODO Assert.assertEquals(0, database.countOfAllElements());

		database.put("alpha01", "qlskjfmlqja");
		database.put("alpha01", "qlskjfmlqjb");
		database.put("alpha02", "qlskjfmlqj");
		database.put("alpha03", "qlskjfmlqj");
		database.put("alpha04", "qlskjfmlqj");
		database.put("alpha05", "qlskjfmlqj");
		database.delete("alpha03");
		database.put("alpha01s", "bravo1", "qlskjfmlqja");
		database.put("alpha01s", "bravo1", "qlskjfmlqjb");
		database.put("alpha01s", "bravo2", "qlskjfmlqj");
		database.put("alpha01s", "bravo3", "qlskjfmlqj");
		database.put("alpha01s", "bravo4", "qlskjfmlqj");
		database.put("alpha01s", "bravo5", "qlskjfmlqj");
		database.delete("alpha01s", "bravo3");

		// System.out.println(database.countOfElements() + " " +
		// database.countOfArchivedElements() + " " +
		// database.countOfAllElements());

		Assert.assertEquals(8, database.countOfElements());
		// TODO Assert.assertEquals(4, database.countOfArchivedElements());
		// TODO Assert.assertEquals(10, database.countOfAllElements());

		database.clear();

		Assert.assertEquals(0, database.countOfElements());
		// TODO Assert.assertEquals(0, database.countOfArchivedElements());
		// TODO Assert.assertEquals(0, database.countOfAllElements());

		//
		logger.debug("===== test done.");
	}

	/**
	 * Test tittle 01.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testTittle01() throws Exception
	{
		//
		logger.debug("===== test starting...");

		database.clear();

		String source = "ME ME MEààààà ıııı éééé";
		database.put("alpha01", source);
		String target = database.getValue("alpha01");
		Assert.assertEquals(source, target);

		//
		logger.debug("===== test done.");
	}

	/**
	 * After class.
	 * 
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	@AfterClass
	public static void afterClass() throws SikevaDBException
	{
		if (database != null)
		{
			database.close();
			database.destroy();
		}
	}

	/**
	 * Before class.
	 * 
	 * @throws SikevaDBException
	 *             the SikevaDB exception
	 */
	@BeforeClass
	public static void beforeClass() throws SikevaDBException
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.DEBUG);

		// Add ?profileSQL=true to generate huge logs.

		// database = new SQLSikevaDB("com.mysql.jdbc.Driver",
		// "jdbc:mysql://localhost:3306/sikevadb-test", "sikevadb-test",
		// "12345678");

		File homeDirectory = new File("/tmp/footest");
		database = new FileTreeSikevaDB(homeDirectory, null, null);

		if (!database.exists())
		{
			database.create();
		}

		database.open();
	}
}
